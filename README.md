# Zapiski za fiziko na FMF 

Zapiski v obliki cheat sheetov za nekatere predmete programa Fizika, Fakulteta
za matematiko in fiziko Univerze v Ljubljani. Verjetno uporabno tudi za podobne
predmete na drugih programih. Lahko vsebuje napake, vesel bom merge requestov
ali emailov na [urban.duh@protonmail.com](mailto:urban.duh@protonmail.com).

Jezik cheat sheeta je odvisen od jezika predavanj, ko sem jih obiskoval jaz. Če
se kdo odloči prevajati (v katero koli smer), z veseljem sprejmem merge
request.

# Notes for FMF Physics course

Notes in the form of cheat sheets for some courses at the Faculty of Mathematics
and Physics, University of Ljubljana, Physics program. May be useful for similar
courses at other programs. Can contain mistakes, merge requests are welcome. You
can also email me at
[urban.duh@protonmail.com](mailto:urban.duh@protonmail.com).


