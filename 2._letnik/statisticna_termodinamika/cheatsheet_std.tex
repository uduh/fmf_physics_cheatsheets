\documentclass[10pt,landscape]{article}
\usepackage{multicol}
\usepackage{calc}
\usepackage{ifthen}
\usepackage[landscape]{geometry}
\usepackage{amsmath,amsthm,amsfonts,amssymb}
\usepackage{color,graphicx,overpic}
\usepackage{hyperref}
\usepackage{amsthm}
\usepackage{mathrsfs}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{physics}

\pdfinfo{
  /Title (Uporabne formule iz statistične termodinamike za fizike)
  /Author (Urban Duh)
  /Subject (Fizika)}

% This sets page margins to .5 inch if using letter paper, and to 1 cm
% if using A4 paper. (This probably isn't strictly necessary.)
% If using another size paper, use default 1cm margins.
\ifthenelse{\lengthtest { \paperwidth = 11in}}
    { \geometry{top=.5in,left=.5in,right=.5in,bottom=.5in} }
    {\ifthenelse{ \lengthtest{ \paperwidth = 297mm}}
        {\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
        {\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
    }

% Turn off header and footer
\pagestyle{empty}

% Redefine section commands to use less space
\makeatletter
\renewcommand{\section}{\@startsection{section}{1}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%x
                                {\normalfont\large\bfseries}}
\renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
                                {-1explus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%
                                {\normalfont\normalsize\bfseries}}
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {1ex plus .2ex}%
                                {\normalfont\small\bfseries}}
\makeatother

% Don't print section numbers
\setcounter{secnumdepth}{0}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0pt plus 0.5ex}

%My Environments
\newcommand{\sprod}[1]{\langle#1\rangle}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Lor}{\underline{\underline L}}
\newcommand{\rr}{\underline r}
\newcommand{\avg}[1]{\langle#1\rangle}
\renewcommand{\vec}{\vb}
% -----------------------------------------------------------------------

\begin{document}
\raggedright
\footnotesize
\begin{multicols}{3}
% multicol parameters
% These lengths are set only within the two main columns
%\setlength{\columnseprule}{0.25pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

\section{Termodinamika}

\subsection{Stanja}

Ekstenzivne spremenljivke $X$: $X_3 = X_1 + X_2$. \\
Intenzivne spremenljivke $Y$: $Y_3 = Y_1 = Y_2$. \smallskip \\

Nabori TDS: $(p, V, T), (F, x, T), (\vec p_m, \vec B, T), (\vec H, \vec M, T)$, $(U, e, T), (\vec E, \vec p_e, T), (\vec E, \vec P, T)$. \medskip \\

$\dd{V} = \left( \frac{\partial V}{\partial T} \right)_p \dd{T} + \left(
\frac{\partial V}{\partial p} \right)_T \dd{p} = V(\beta \dd{T} - \chi_T \dd{p})$ \\
$\beta = \frac{1}{V} \left( \frac{\partial V}{\partial T} \right)_p$ \qquad $\chi_T = - \frac{1}{V} \left( \frac{\partial V}{\partial p} \right)_T$ \\
$V_M = \frac{MV}{m}$\medskip \\

\textbf{Idealni plin:} $pV = \frac{m}{M} R T$ \\
\textbf{Realni plin:} $\left( p + \frac{a}{V_M^2} \right) \left( V_m - b \right) = RT$ (van der Waals) \\
Brezdimenzijska oblika: $\left( \mathcal{P} + \frac{3}{\mathcal{V}^2} \right) \left( 3 \mathcal{V} - 1 \right) = 8 \mathcal{T}$ \\
$\mathcal{P} = \frac{p}{p_c} = \frac{27 b^2 p}{a}$, \quad $\mathcal{V} = \frac{V_m}{V_m^c} = \frac{V_m}{3b}$, \quad $\mathcal{T} = \frac{T}{T_c} = \frac{27 R bT}{8 a}$ \medskip \\

\textbf{Dielektrik:} $D =  \varepsilon \varepsilon_0 E = \varepsilon_0 E + P$ \\
$P = \chi \varepsilon_0 E$ \\

\textbf{Paramagnet:} $M = \frac{a H}{T}$ \qquad (Curiejev zakon) \\
$B = \mu \mu_0 H = \mu_0 (H + M)$ \\
$M = \chi H$ \\

\textbf{Superprevodnik:} \\
$\chi = -1$ v superprevodni fazi \\
$\chi = 0$ v normalni fazi \medskip \\

\subsection{Energijski zakon}

$\dd{U} = \dd{Q} + \dd{W}$ \medskip \\

$H = U + pV$ \\
$\dd{H} = V \dd{p} + \dd{Q}$ \medskip \\

$\dd{W} = - p \dd{V} = \mu_0 H \dd{(MV)} = E \dd{(PV)}$ \medskip \\

$C_V = \left( \frac{\partial U}{\partial T} \right)_V$ \\
$C_p = \left( \frac{\partial H}{\partial T} \right)_p$ \medskip \\

\textbf{Ideali plin:} \\
$U = m c_V T$ \\
$H = m c_p T$ \\

\subsection{Entropijski zakon}

$\Delta S \geq \int \frac{\dd{Q}}{T}$ (enakost pri reverzibilnih) \medskip \\

$\eta_c = 1 - \frac{T_2}{T_1}$ (Carnotov toplotni stroj) \medskip \\

\textbf{Idealni plin:} \\
$\Delta S = m c_v \ln \frac{T}{T_0} + \frac{mR}{M} \ln \frac{V}{V_0} = m c_p \ln \frac{T}{T_0} - \frac{mR}{M} \ln \frac{p}{p_0}$ \medskip \\

\subsection{Termodinamični potenciali}

$F = U - TS$ (prosta energija) \\
$\dd{F} \leq -p \dd{V} - S \dd{T}$ \smallskip \\

$G = H - TS$ (prosta entalpija) \\
$\dd{G} \leq V \dd{p} - S \dd{T}$ \medskip \\

$\left(\frac{\partial S}{\partial V}\right)_T = \left(\frac{\partial p}{\partial T}\right)_V = \frac{\beta}{\chi_T}$ (I. Maxwellova relacija) \\
$\left(\frac{\partial S}{\partial p}\right)_T = -\left(\frac{\partial V}{\partial T}\right)_p = - \beta V$ (II. Maxwellova relacija) \medskip \\

$\chi_S = - \frac{1}{V} \left(\frac{\partial V}{\partial p}\right)_S = \frac{\chi_T}{\kappa}$ \\
$c_p - c_V = \frac{T \beta^2}{\rho \chi_T}$ \\
$\left(\frac{\partial T}{\partial p}\right)_H = \frac{\beta T - 1}{\rho c_p}$ (Joule - Kelvinov koeficient) \medskip \\

\subsection{Fazne spremembe}

$\mu_i = \left(\frac{\partial G}{\partial m_i}\right)_{T, p}$ (kemijski potencial) \\
Pri enofaznem sistemu $\mu = \frac{G}{m}$ \\
Pri faznem ravnovesju $\mu_p = \mu_k$ \smallskip \\

$q_i = -\frac{T}{m} \left[\left(\frac{\partial g}{\partial T}\right)_{p, plin} - \left(\frac{\partial g}{\partial T}\right)_{p, kaplj}\right]$  \medskip \\

\textbf{Clausius - Clapeyronova enačba} \\
$\dv{p_s}{T} = \frac{\Delta S}{\Delta V} = \frac{q_i}{T(\rho_p^{-1} - \rho_k^{-1})}$ \\
$p_s(T) = p_s(T') \exp\left(-\frac{M q_i}{R} \left(\frac{1}{T} - \frac{1}{T'}\right)\right)$ \medskip \\

\subsection{Transportni pojavi}

$\frac{\partial \rho_i}{\partial t} = - \div \vec j_i$ \\
$\vec j_i = - D \, \grad \rho_i$ \\
$\frac{\partial \rho_i}{\partial t} = D \, \nabla^2 \rho_i$ \smallskip \\

Rešitev za $\rho_1(z, t = 0) = c \delta(z)$: \\
$\rho_1(z, t) = \frac{m_1}{A \sqrt{4 \pi D t}} \exp\left(-\frac{z^2}{4Dt}\right)$ \medskip \\

$\frac{\partial T}{\partial t} = - \frac{1}{\rho c_p} \div \vec j_Q$ \\
$\vec j_Q = - \lambda \, \grad T$ \\
$\frac{\partial T}{\partial t} = \frac{\lambda}{\rho c_p} \, \nabla^2 T$ \\
\textbf{S toplotnimi izvori/ponori:} $q = \frac{\partial P}{\partial V}$ \\
$\frac{\partial j}{\partial z} = q - \rho c_p \frac{\partial T}{\partial t}$ \medskip \\

\textbf{Termočlen:} $U = a \Delta T$ \\
\textbf{Peltierjev pojav:} $P = \pi I$ \\
$\dv{\pi}{T} = a - (\sigma_A - \sigma_B)$ $\implies$ $\pi \approx a T$ \medskip \\



\section{Klasična statistična fizika}

V ravnovesnem stanju $\rho = \rho(E(\vec r, \vec p))$ \smallskip \\

\textbf{Mikrokanonična porazdelitev}: \\
$\sum E_i = \mathrm{konst.}$ \\
$\int \rho(E) \dd{\Gamma} = \int \rho(E) g(E) \dd{E} = 1$ \\
$\rho = \begin{cases}
(g(E) \Delta E)^{-1} & E_0 - \Delta E < E < E_0 + \Delta E \\
0 & \mathrm{sicer} \\
\end{cases}$ \medskip \\

\textbf{Klasična kanonična porazdelitev}: \\
$T = \mathrm{konst.}$ \\
$\rho(E) \propto \exp[-\beta (E - F)]$ \\
$\beta = \frac{1}{k_B T}$ \medskip \\

\textbf{Fazni integral/vsota}: \\
$Z = \exp(- \beta F) = C \int \exp(- \beta E) \dd{\Gamma}$ \\
$ \avg Y = C \int Y(\vec r, \vec p, \dots) \exp[-\beta (E - F)] \dd{\Gamma}$ \\
$\avg E = \dv{(\beta F)}{\beta}$ \medskip 

\textbf{Ekviparticijski izrek}: \\
Povprečna energija vsake kvadratične, neomejene in neodvisne prostorske stopnje je enaka $\frac{1}{2} k_B T$. \medskip \\

\textbf{Fluktuacije energije}: \\
$\sigma_E^2 = \avg{E^2} -  \avg{E}^2 = -\dv[2]{(\beta F)}{\beta} = k_B T^2 C_V \propto N$ \medskip \\


\subsection{Enačba stanja}

$p = - \left(\frac{\partial F}{\partial V}\right)_\beta$ \medskip \\

\textbf{Virialni razvoj}: \\
$\frac{\beta p V}{N} = 1 + \sum_{i = 2}^{\infty} B_i \left(\frac{N}{V}\right)^{i - 1}$ \\
Če vzamemo samo $i = 2:$ \\
$e^{-\beta F} = C \left(\frac{2 \pi m}{\beta}\right)^\frac{3N}{2} I$ \\
$I = V^N(1-\frac{N^2 B_2}{V})$ \\
$\frac{N^2 B_2}{V} \ll 1 \implies \ln I \approx N \ln V - B_2 \frac{N^2}{V}$ \\
$B_2 = \frac{1}{2} \int \left[1 - e^{- \beta \phi(\vec r_{i, j})}\right] \dd{\vec r_{i, j}}$ \\
V sferičnih koordinatah: $B_2 = \frac{1}{2} \int_0^\infty \left[1 - e^{- \beta
\phi(r)}\right] 4 \pi r^2 \dd{r} $  \\
$p = \frac{N}{V \beta} + \frac{N^2 B_2}{V^2 \beta}$ \medskip \\

\subsection{Entropija}

$S = \frac{\avg{E} - F}{T}$ \\
$S = k_B \avg{\beta(E - F)}$ \\
Kanonična porazdelitev (Gibbs): \\
$S = - k_B \avg{\ln \frac{\rho}{C}} = -k_B \int \rho \ln \frac{\rho}{C}
\dd{\Gamma}$ \smallskip \\
Mikrokanonična porazdelitev (Boltzmann): \\
$S = k_b \ln(\Delta \Gamma C)$ \medskip \\

\section{Kvantna statistična fizika}

Bozoni imajo cel spin. \\
Fermioni imajo polovičen spin, za njih velja Paulijevo načelo. \\
$\Psi^F(\vec r_1 \dots \vec r_n) \propto \sum_\sigma \mathrm{sgn}(\sigma) \varphi_\alpha(\vec r_\sigma(1)) \varphi_\beta(\vec r_\sigma(2)) \cdots $ \\
$\Psi^B(\vec r_1 \dots \vec r_n) \propto \sum_\sigma \varphi_\alpha(\vec r_\sigma(1)) \varphi_\beta(\vec r_\sigma(2)) \cdots $ \medskip \\

$\oint p \dd{q} = n h$ \quad (Bohr - Sommerfeldovo kvantizacijsko pravilo) \\
$C = \frac{(2j + 1)^N}{N! h^{3N}}$ \medskip \\

Enačbe za fazno vsoto veljajo tudi tukaj, le da seštevamo po diskretnih stanjih. \medskip \\

\subsection{Paramagnetizem}

$p_{m_z} = \gamma \hbar j_z = g \mu_B j_z$ \\
$j_z = -j, -j +1 \dots j - 1, j$ \quad ($(2j + 1)$ kratna degeneracija) \\
$E = - \vec p_m \cdot \vec B = - p_{m_z} B$ \medskip \\

\textbf{Isingov model} \\

$H = - J \sum_{i, j \, \mathrm{soseda}} s_{i_z} s_{j_z} = - \sum_i \gamma \hbar s_{i_z} \sum_{j \, \mathrm{sosed} \, i} \frac{J s_{j_z}}{\gamma \hbar} =$ \\$= - \sum_i \gamma \hbar s_{i_z} B_{\mathrm{lok}_i}$ \\
$\avg{B_\mathrm{lok}} = z \frac{J \avg{s_z}}{\gamma \hbar}$ \quad (približek povprečnega polja) \\

\section{Velekanonična porazdelitev}

$\rho(E, N) \propto \exp\left[ \beta \mu N - \beta E \right]$ \\
$C_N = \frac{(2j + 1)^N}{N! h^{3N}}$ \\
$Z = \exp\left[- \beta q \right] = \sum_{N = 0}^{\infty} C_N \int \exp\left[
\beta (\mu N - E) \right] \dd{\Gamma_N}$ \medskip \\

$ \avg Y = \sum_{N = 0}^{\infty} C_N \int Y(\vec r, \vec p, \dots) \exp\left[
\beta (\mu N - E + q) \right] \dd{\Gamma_N}$ \\
$\avg E = \dv{(\beta q)}{\beta}$ \\ 
$\avg p = - \left(\frac{\partial q}{\partial V}\right)_{\beta, \mu} = - \left(\frac{\partial (\beta q)}{\partial (\beta V)}\right)_{\beta, \mu}$ \\
$\avg N = - \left( \frac{\partial (\beta q)}{\partial (\beta \mu)} \right)_\beta$ \\
$q = -pV$ \medskip \\

Enoatomni idealni plin: \\
$e^{\beta \mu} = \frac{h^3}{(2j + 1) (2 \pi m)^\frac{3}{2} k_B^\frac{5}{2}} \frac{p}{T^\frac{5}{2}} = \frac{1}{J} \frac{p}{T^\frac{5}{2}}$ \medskip \\

\textbf{Fermi - Diracova porazdelitev} \\

$e^{- \beta q} = \prod_j \left[1 + \exp\left( \beta \mu - \beta E_j \right)\right]$ \\
$\avg N = \sum_j \frac{1}{\exp\left( \beta E_j - \beta \mu \right) + 1}$ \medskip \\

\textbf{Bose - Einsteinova porazdelitev} \\

$e^{- \beta q} = \prod_j \frac{1}{1 - \exp\left( \beta \mu - \beta E_j \right)}$ \\
$\avg N = \sum_j \frac{1}{\exp\left( \beta E_j - \beta \mu \right) - 1}$ \medskip \\


\section{Kinetična teorija plinov}

$p = n k_B T$ \\
$\rho(v) = \left(\frac{m}{2 \pi k_B T}\right)^\frac{3}{2} \exp\left[-\frac{m v^2}{2 k_B T}\right]$ \\
$\avg{v} = \sqrt{\frac{8 k_B T}{\pi m}}$ \medskip \\

$j = \frac{n \avg{v}}{4}$ \\
$l_p = (\sqrt{2} \pi \sigma^2 n)^{-1}$ \\
$D = \frac{6}{10} \avg{v} l_p$ \\
$\eta = \frac{1}{2} \rho_m \avg{v} l_p$ \\
$\lambda = \frac{3}{4} n \avg{v} l_p k_B$ \medskip \\

\vfill\null
\columnbreak
\vfill\null
\columnbreak
\vfill\null
\columnbreak

\end{multicols}
\end{document}
