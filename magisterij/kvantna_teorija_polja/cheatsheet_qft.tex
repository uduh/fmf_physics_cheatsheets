% !TeX spellcheck = en_GB

\documentclass[10pt,landscape]{article}
\usepackage{multicol}
\usepackage{calc}
\usepackage{ifthen}
\usepackage[landscape]{geometry}
\usepackage{amsmath,amsthm,amsfonts,amssymb}
\usepackage{color,graphicx,overpic}
\usepackage{hyperref}
\usepackage{amsthm}
\usepackage{mathrsfs}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{icomma}
\usepackage{soul}
\usepackage{physics}
\usepackage{slashed}
\usepackage{simpler-wick}
\usepackage{tikz-feynman}

\pdfinfo{
	% /Title (Uporabne formule iz kvantne teorije polja)
	% /Author (Urban Duh)
	% /Subject (Kvantna teorija polja)}

% This sets page margins to .5 inch if using letter paper, and to 1 cm
% if using A4 paper. (This probably isn't strictly necessary.)
% If using another size paper, use default 1cm margins.
\ifthenelse{\lengthtest { \paperwidth = 11in}}
{ \geometry{top=.5in,left=.5in,right=.5in,bottom=.5in} }
{\ifthenelse{ \lengthtest{ \paperwidth = 297mm}}
	{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
	{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
}

% Turn off header and footer
\pagestyle{empty}

% Redefine section commands to use less space
\makeatletter
\renewcommand{\section}{\@startsection{section}{1}{0mm}%
	{-1ex plus -.5ex minus -.2ex}%
	{0.5ex plus .2ex}%x
	{\normalfont\large\bfseries}}
\renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
	{-1explus -.5ex minus -.2ex}%
	{0.5ex plus .2ex}%
	{\normalfont\normalsize\bfseries}}
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
	{-1ex plus -.5ex minus -.2ex}%
	{1ex plus .2ex}%
	{\normalfont\small\bfseries}}
\makeatother

% Don't print section numbers
\setcounter{secnumdepth}{0}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0pt plus 0.5ex}

% My Environments
\newcommand{\1}{\text{\usefont{U}{bbold}{m}{n}1}}
% -----------------------------------------------------------------------

\begin{document}
\raggedright
\footnotesize
\begin{multicols}{3}

$c = \hbar = 1$ \\
$g_{\mu\nu} = \mathrm{diag}(1, -1, -1, -1)$ \medskip

\section{Klasična teorija polja}

$L = \int \dd[3]{x} \mathcal{L}(\phi, \partial_\mu \phi)$ \qquad (Lagrangian) \\
$S = \int \dd{t} L = \int \dd[4]{x} \mathcal{L}(\phi, \partial_\mu \phi)$ \qquad
(akcija) \\
Enačbe gibanje ustrezajo ekstremu $S$: \\
$\pdv{\mathcal L}{\phi} - \partial_\mu \qty(\pdv{\mathcal{L}}{(\partial_\mu
\phi)}) = 0$ \medskip \\

$\pi(\vb x) = \pdv{\mathcal L}{\dot \phi(\vb x)}$ \qquad (prostorska gostota
posplošenega momenta) \\
$\mathcal{H}(\vb x) = \pi(\vb x) \dot \phi(\vb x) - \mathcal{L}$ \qquad
(prostorska gostota Hamiltoniana) \medskip \\

\subsection{Izrek Emmy Noether}

Če so enačbe gibanja invariantne na infinitezimalno globalno zvezno
transformacijo $\phi \to \phi + \alpha \Delta \phi$, pri kateri $\mathcal L \to
\mathcal L + \alpha \partial_\mu \mathcal J^\mu$, potem:
\vspace{-0.10cm}
\begin{itemize}[itemindent=-13pt]
	\itemsep-0.1em 
    \item $j^\mu = \frac{\partial \mathcal L}{\partial \qty(\partial_\mu \phi)} \Delta \phi - \mathcal
        J^\mu$, \quad $\partial_\mu j^\mu = 0$, \\
    \item $Q = \int j^0 \dd[3]{\vb x}$, \qquad $\dv{Q}{t} = 0$.
\end{itemize}
\medskip

$T^\mu_{}_\nu = \pdv{\mathcal{L}}{\qty(\partial_\mu \phi)} \partial_\nu \phi -
\mathcal L \delta^\mu_{}_\nu$ \qquad (tenzor $E$ in $p$) \\
$\partial_\mu T^\mu_{}_\nu = 0$ \\
$\mathcal H = T^{00}$ \\
$P^i = \int T^{0i} \dd[3]{\vb x} = - \int \pi \partial_i \phi \dd[3]{\vb x}$
\qquad (gibalna količina) \medskip \\

\section{Prosta polja}

\subsection{Lorentzove transformacije polj}

Enačba gibanja je invariantna na Lorentzove transformacije, če iz tega, da je
$\phi$ rešitev enačbe sledi, da je tudi Lorentzova transformiranka $\phi'$
rešitev iste enačbe. \\
Enačba gibanja, ki sledi iz Lagrangiana, je Lorentzovo invariantna, če je
Lagrangian Lorentzov skalar ($\mathcal{L}(x) \rightarrow
\mathcal{L}(\Lambda^{-1}x)$). \medskip \\

$x^\mu \rightarrow x^{\mu'} = \Lambda^{\mu'}_{}_\nu x^\nu$ \\
$\phi(x) \rightarrow \phi'(x) = \phi(\Lambda^{-1} x)$ \\
$V^\mu(x) \rightarrow V^{\mu'}(x) = \Lambda^{\mu'}_{}_{\nu}
V^\nu(\Lambda^{-1}x)$ \medskip \\

$S^{\mu\nu} = \frac{i}{4}\comm{\gamma^\mu}{ \gamma^\nu}$ \\
$\Lambda_{1/2} = \exp(-\frac{i}{2} \omega_{\mu\nu}S^{\mu\nu})$ \\
$\psi(x) \rightarrow \psi'(x) = \Lambda_{1/2} \psi(\Lambda^{-1}x)$ \medskip \\

$S^{0i} = -\frac{i}{2} \mqty(\sigma^i & 0 \\ 0 & - \sigma^i)$ \\
$S^{ij} = \frac{1}{2} \varepsilon^{ijk} \mqty(\sigma^k & 0 \\ 0 & \sigma^k)$ \medskip \\

$\qty(\mathcal{J^{\mu \nu}})_{\alpha \beta} = i \qty(\delta^\mu_{}_\alpha
\delta^\nu_{}_{\beta} - \delta^\mu_{}_\beta \delta^\nu_{}_\alpha)$ \medskip \\
$\Lambda^\mu_{}_\nu = \exp(-\frac{i}{2} \omega_{\alpha\beta}
\qty(\mathcal{J}^{\alpha\beta})^\mu_{}_\nu)$ \medskip \\

$\comm{\gamma^\mu}{ S^{\rho \sigma}} = \qty(\mathcal{J}^{\rho\sigma})^\mu_{}_\nu
\gamma^\nu$ \\
$\Lambda^{-1}_{1/2} \gamma^\mu \Lambda_{1/2} = \Lambda^\mu_{}_\nu \gamma^\nu$
\medskip \\

\subsubsection{Lorentzove transformacije operatorjev polj}

$\hat \phi(x) \to \hat \phi(\Lambda x)$ \\
$\hat V^\mu(x) \to \qty(\Lambda^{-1})^\mu_{}_\nu \hat V^\nu(\Lambda x)$ \\
$\hat \psi(x) \to \Lambda_{1/2}^{-1} \hat \psi(\Lambda x)$ \medskip \\

\subsection{Realno skalarno polje}

$\mathcal{L} = \frac{1}{2}\partial_\mu \phi \partial^\mu \phi - \frac{1}{2} m^2
\phi^2$ \\
$\implies$ $\qty(\partial_\mu \partial^\mu + m^2)\phi = 0$ \qquad (Klein-Gordonova
enačba) \\
$\pi = \dot \phi$ \\
$\mathcal{H} = \frac{1}{2}\qty(\dot \phi^2 + (\grad \phi)^2 + m^2 \phi^2)$
\medskip \\

\subsubsection{Kvantizacija}

Polja in opazljivke spremenimo v operatorje $\phi \to \hat \phi$ (vendar $\hat
\cdot$ v notaciji spustimo). \medskip \\

Kanonične komutacijske relacije: \\
$\comm{\phi(\vb x)}{ \pi(\vb y)} = i \delta(\vb x - \vb y)$ \\
$\comm{\phi(\vb x)}{ \phi(\vb y)} = \comm{\pi(\vb x)}{ \pi(\vb y)} = 0$ \medskip
\\

$\phi(\vb x) = \int \frac{\dd[3]{\vb p}}{(2 \pi)^3} \frac{1}{\sqrt{2E_{\vb p}}}
\qty(a_{\vb p} e^{-i \vb p \cdot \vb x} + a_{\vb p}^\dagger e^{i \vb p \cdot \vb
x})$ \\
$\phi(\vb x, t) = \int \frac{\dd[3]{\vb p}}{(2 \pi)^3} \frac{1}{\sqrt{2E_{\vb p}}}
\eval{\qty(a_{\vb p} e^{-i p \cdot x} + a_{\vb p}^\dagger e^{i p \cdot
x})}_{p^0 = E_{\vb p}}$ \\
$E_{\vb p} = \sqrt{\vb p^2 + m^2}$ \medskip \\

$\comm{a_{\vb p}}{ a^\dagger_{\vb q}} = (2\pi)^3 \delta(\vb p - \vb q)$ \\
$\comm{a_{\vb p}}{ a_{\vb q}} = \comm{a^\dagger_{\vb p}}{ a^\dagger_{\vb q}} =
0$ \medskip \\

$\pi(\vb x, t) = \int \frac{\dd[3]{\vb p}}{(2\pi)^3}
\qty(-i\sqrt{\frac{E_{\vb p}}{2}})\eval{\qty(a_{\vb p} e^{-i p \cdot x} - a_{\vb
    p}^\dagger e^{i p \cdot x})}_{p^0 = E_{\vb p}}$ \\
$\1 = \int \frac{\dd[3]{\vb p}}{(2\pi)^3} \frac{1}{2E_{\vb p}} \ket{\vb
p}\bra{\vb p}$ \medskip \\

\subsubsection{Opazljivke}

$H = \int \frac{\dd[3]{\vb p}}{(2\pi)^3} E_{\vb p} \qty(a_{\vb p{^\dagger}} a_{\vb
p} + \frac{(2 \pi)^3}{2} \delta(0)) =$ \\\hspace{10.5pt}$= \int \frac{\dd[3]{\vb p}}{(2\pi)^3}
E_{\vb p} \qty(a_{\vb p{^\dagger}} a_{\vb p} + E_0)$ \\
$\vb P = \int \frac{\dd[3]{\vb p}}{(2\pi)^3} \vb p a_{\vb p}^\dagger a_{\vb p}$
\medskip \\

\subsubsection{Stanja}

$a_{\vb p} \ket{0} = 0$ \qquad (vakuum) \\
$\braket{0}{0} = 0$ \medskip \\

$\ket{\vb p} = \sqrt{2E_{\vb p}} a_{\vb p}^\dagger \ket{0}$ \qquad (Lorentzovo
inv.\ enodelčno stanje) \\
$\braket{\vb p}{\vb q} = 2E_{\vb p} (2\pi)^3 \delta(\vb p - \vb q)$ \\
$\vb P \ket{\vb p} = \vb p \ket{\vb p}$ \medskip \\

$\ket{\vb x} = \phi(\vb x)\ket{0}$ \medskip \\

\subsubsection{Korelator in propagator}

$D(x - y) = \ev{\phi(x) \phi(y)}{0} = \int \frac{\dd[3]{\vb p}}{(2\pi)^3}
\frac{1}{2E_{\vb p}} \eval{e^{-ip \cdot(x - y)}}_{p^0 = E_{\vb p}}$ \medskip \\

$D_F(x - y) = \ev{T\qty(\phi(x) \phi(y))}{0} = \begin{cases}
    D(x - y); &x^0 > y^0 \\
    D(y - x); &x^0 < y^0 \\
\end{cases} = $ \\ \hspace{42pt}$= \int \frac{\dd[4]{p}}{(2\pi)^4} \frac{i}{p^2 - m^2 + i
\varepsilon} e^{-i p \cdot(x - y)}$, \\
kjer $\varepsilon \searrow 0$.  \qquad (Feynmanov propagator) \\
$(\partial^2 + m^2) D_F(x - y) = -i \delta(x - y)$ \medskip \\

\subsubsection{Kompleksno skalarno polje}

$\phi(\vb x, t) = \int \frac{\dd[3]{\vb p}}{(2 \pi)^3} \frac{1}{\sqrt{2E_{\vb p}}}
\eval{\qty(a_{\vb p} e^{-i p \cdot x} + b_{\vb p}^\dagger e^{i p \cdot
x})}_{p^0 = E_{\vb p}}$, \\
kjer so $b$ operatorji definirano analogno kot pri Diracovem polju. \medskip \\

\subsection{Diracovo polje}

$\acomm{\gamma^\mu}{ \gamma^\nu} = 2 g^{\mu \nu}\1_{4 \times 4}$ \\
Delali bomo v Weylovi reprezentaciji: \\
$\gamma^0 = \mqty(0 & \1 \\ \1 & 0)$, \qquad $\gamma^i = \mqty(0 & \sigma^i \\
-\sigma^i & 0)$ \medskip \\

$\bar \psi = \psi^\dagger \gamma^0$ \\
$\bar \psi \psi$ je Lorentzov skalar. \medskip \\

$\mathcal{L} = \bar \psi (i \gamma^\mu \partial_\mu - m) \psi$ \\
$\implies$ $(i \gamma^\mu \partial_\mu - m)\psi(x) = 0$ \medskip \\

$\pi = i \psi^\dagger$ \\
$\mathcal H = \psi^\dagger \qty(-i\gamma^0 \boldsymbol \gamma \cdot \grad + m
\gamma^0) \psi$ \medskip \\

\subsubsection{Rešitve Diracove enačbe}

$\sigma^\mu = (1, \boldsymbol \sigma)$ \\
$\bar \sigma^\mu = (1, - \boldsymbol \sigma)$ \\
$\xi^1 = \mqty(1 \\ 0)$, \qquad $\xi^2 = \mqty(0 \\ 1)$ \medskip \\

$\psi(x) = u(p)e^{-ip \cdot x}$ \\
$p^2 = m^2$, \qquad $p^0 > 0$ \\
$u^s = \mqty(\sqrt{p \cdot \sigma} \xi^s \\ \sqrt{p \cdot \bar \sigma}
\xi^s)$, \qquad $s = 1, 2$ \\
$\bar u^r(p) u^s(p) = 2 m \delta^{rs}$ $\iff$ $u^r^\dagger(p) u^s(p) = 2 E_{\vb
p} \delta^{rs}$ \medskip \\

$\psi(x) = v(p)e^{ip \cdot x}$ \\
$p^2 = m^2$, \qquad $p^0 > 0$ \\
$v^s = \mqty(\sqrt{p \cdot \sigma} \eta^s \\ -\sqrt{p \cdot \bar \sigma}
\eta^s)$, \qquad $s = 1, 2$ \\
$\bar v^r(p) v^s(p) = -2 m \delta^{rs}$ $\iff$ $v^r^\dagger(p) v^s(p) = 2 E_{\vb
p} \delta^{rs}$ \medskip \\
$\eta^s$ izberemo tako, da: \\
$\bar u^r(p)v^s(p) = \bar v^r(p) u^s(p) = 0$ \medskip \\

\subsubsection{Uporabne identitete}

$\sum_s u^s(p) \bar u^s(p) = \slashed p + m$ \\
$\sum_s v^s(p) \bar v^s(p) = \slashed p - m$ \medskip \\

${\gamma^0}^\dagger = \gamma^0$, \qquad \qquad ${\boldsymbol \gamma}^\dagger = -\boldsymbol 
\gamma^k$ \\
${\gamma^\mu}^\dagger = \gamma^0 \gamma^\mu \gamma^0$ \\
$\tr \gamma^\mu = 0$ \medskip \\

$\gamma^5 = i \gamma^0 \gamma^1 \gamma^2 \gamma^3 = \mqty(-\1 & 0 \\ 0 & \1)$ \\
$\acomm{\gamma^5}{\gamma^\mu} = 0$ \\
${\gamma^5}^\dagger = \gamma^5$ \\
$\qty(\gamma^5)^2 = 1$ \\
$\tr \gamma^5 = 0$ \medskip \\

$\tr[\gamma^\mu \gamma^\nu] = 4 g^{\mu \nu}$ \\
$\tr[\gamma^\mu \gamma^\nu \gamma^\lambda] = 0$ \qquad (enako za produkte lihega števila) \\
$\tr[\gamma^\mu \gamma^\nu \gamma^\alpha \gamma^\beta] = 4 \qty(g^{\mu \nu} g^{\alpha \beta} - 
g^{\mu \alpha} g^{\nu \beta} + g^{\mu \beta} g^{\nu \alpha})$ \\
$\tr[\gamma^\mu \gamma^5] = \tr[\gamma^\mu \gamma^\nu \gamma^\sigma \gamma^5] = 0$ \qquad (enako za 
liho število $\gamma^\mu$) \\
$\tr[\gamma^\mu \gamma^\nu \gamma^5] = 0$ \\
$\tr[\gamma^\mu \gamma^\nu \gamma^\alpha \gamma^\beta \gamma^5] = -4i \varepsilon^{\mu \nu \alpha 
\beta}$ \medskip \\

\subsubsection{Kvantizacija}

Polja in opazljivke spremenimo v operatorje. \medskip \\

Kanonične antikomutacijske relacije: \\
$\acomm{\psi_a(\vb x)}{ \pi_b(\vb y)} = i\delta(\vb x - \vb y)
\delta_{ab}$ \\
$\acomm{\psi_a(\vb x)}{ \psi_b(\vb y)} = \acomm{\pi_a(\vb x)}{ \pi_b(\vb y)} =
0$ \medskip \\

$\psi(x) = \int \frac{\dd[3]{\vb p}}{(2\pi)^3} \frac{1}{\sqrt{2 E_{\vb p}}}
\sum_s \qty(a_{\vb p}^s u^s(p) e^{-i p \cdot x} + b_{\vb p}^s^\dagger v^s(p)
e^{i p \cdot x})$ \medskip \\

$\acomm{a_{\vb p}^r}{ a_{\vb q}^s^\dagger} = \acomm{b_{\vb p}^r}{ b_{\vb
q}^s^\dagger} = (2\pi)^3 \delta(\vb p - \vb q) \delta^{rs}$ \\
$\acomm{a_{\vb p}^r}{ a_{\vb q}^s} = \acomm{a_{\vb p}^r^\dagger}{ a_{\vb
q}^s^\dagger} = \acomm{b_{\vb p}^r}{ b_{\vb q}^s} = \acomm{b_{\vb p}^r^\dagger}{
b_{\vb q}^s^\dagger} = 0$ \medskip \\

\subsubsection{Opazljivke}

$H = \int \frac{\dd[3]{\vb p}}{(2\pi)^3} \sum_s E_{\vb p}\qty(a_{\vb
p}^s^\dagger a_{\vb p}^s + b_{\vb p}^s^\dagger b_{\vb p}^s + E_0)$ \\
$\vb P = \int \frac{\dd[3]{\vb p}}{(2\pi)^3} \sum_s \vb p \qty(a_{\vb
p}^s^\dagger a_{\vb p}^s + b_{\vb p}^s^\dagger b_{\vb p}^s)$ \\
$j^\mu = - \bar \psi \gamma^\mu \psi$ \\
$Q = e \int \frac{\dd[3]{\vb p}}{(2\pi)^3} \sum_s \qty(a_{\vb
p}^s^\dagger a_{\vb p}^s + b_{\vb p}^s b_{\vb p}^s^\dagger) =$ \\ \hspace{6.5pt} $= e \int
\frac{\dd[3]{\vb p}}{(2\pi)^3} \sum_s \qty(a_{\vb p}^s^\dagger a_{\vb p}^s -
b_{\vb p}^s^\dagger b_{\vb p}^s + e_{\ket{0}})$ \medskip \\

$\boldsymbol \Sigma = \mqty(\boldsymbol \sigma & 0 \\ 0 & \boldsymbol \sigma)$
\\
$\vb J = \int \dd[3]{\vb x} \psi^\dagger \qty(\vb x \times (- i\grad) +
\frac{1}{2} \boldsymbol \Sigma) \psi$ \medskip \\

\subsubsection{Stanja}

$\ket{\vb p, s} = \sqrt{2 E_{\vb p}} a_{\vb p}^s^\dagger \ket{0}$ \\
$\braket{\vb p, r}{\vb q, s} = 2E_{\vb p}(2\pi)^3 \delta(\vb p - \vb
q)\delta^{rs}$ \medskip \\

$H a_{\vb p}^s^\dagger \ket{0} = (E_{\vb p} + E_0)\vb p a_{\vb p}^s^\dagger \ket{0}$,
\quad $H b_{\vb p}^s^\dagger \ket{0} = (E_{\vb p} + E_0) b_{\vb p}^s^\dagger \ket{0}$\\
$\vb P a_{\vb p}^s^\dagger \ket{0} = \vb p a_{\vb p}^s^\dagger \ket{0}$,
\quad $\vb P b_{\vb p}^s^\dagger \ket{0} = \vb p b_{\vb p}^s^\dagger \ket{0}$\\
$Qa_{\vb p}^s^\dagger \ket{0} = (e + e_{\ket{0}}) a_{\vb p}^s^\dagger \ket{0}$,
\quad $Q b_{\vb p}^s^\dagger \ket{0} = (-e + e_{\ket{0}}) b_{\vb p}^s^\dagger \ket{0}$\\
$J_z a_{\vb 0}^s^\dagger \ket{0} = \pm \frac{1}{2} a_{\vb 0}^s^\dagger \ket{0}$,
\quad $J_z b_{\vb 0}^s^\dagger \ket{0} = \mp \frac{1}{2} b_{\vb 0}^s^\dagger
\ket{0}$, \quad $s = 1, 2$ \medskip \\

\subsubsection{Propagator}

$S_F(x - y) = \mel{0}{T\qty(\psi(x) \bar \psi(y))}{0} = $\\ \hspace{40.5pt}$ = \begin{cases}
    \mel{0}{\psi(x)\bar \psi(y)}{0}; & x^0 > y^0 \\
    -\mel{0}{\bar \psi(y) \psi(x)}{0}; & x^0 < y^0 \\
\end{cases} = $ \\ \hspace{38pt} $= \int
\frac{\dd[4]{p}}{(2\pi)^4} \frac{i\qty(\slashed p + m)}{p^2 - m^2 + i
\varepsilon} e^{- i p \cdot(x - y)}$, \\
kjer $\varepsilon \searrow 0$. \\
$\qty(i \slashed \partial - m) S_F(x - y) = \delta(x - y)$ \medskip \\

\subsection{Vektorsko polje}

$F^{\mu \nu} = \partial^\mu A^\nu - \partial^\nu A^\mu$ \\
$\mathcal{L} = -\frac{1}{4}F_{\mu \nu}F^{\mu \nu}$ \\
$\implies$ $\partial_\mu F^{\mu \nu} = 0$ \medskip \\

Lorentzeva umeritev: $\partial_\mu A^\mu = 0$ \\
$\implies$ $\partial_\mu \partial^\mu A_\nu = 0$ \medskip \\

Pri rešitvi $A_\mu = \varepsilon_\mu(k) e^{-ik \cdot x}$, je dovolj, da delamo
le s transverzalnimi polarizacijami $\varepsilon^\mu = (0, \boldsymbol
\varepsilon)$, $\boldsymbol \varepsilon \cdot \vb p = 0$. \medskip \\

Feynmanova $\xi = 1$ umeritev (brez tega je $\pi^0 = 0$): \\
$\mathcal L = -\frac{1}{4} F_{\mu \nu} F^{\mu \nu} - \frac{1}{2\xi}
\qty(\partial_\mu A^\mu)^2$ \\
EL enačbe ostanejo enake. \medskip \\

$\pi^\mu = (-\dot A^0, E^i)$ \medskip \\

\subsubsection{Kvantizacija}

Polja in opazljivke spremenimo v operatorje. \medskip \\

Kanonične komutacijske relacije: \\
$\comm{A^\mu(\vb x)}{ \pi^\nu(\vb y)} = i \delta^{\mu \nu} \delta(\vb x - \vb y)$ \\
$\comm{A^\mu(\vb x)}{ A^\mu(\vb y)} = \comm{\pi^\nu(\vb x)}{ \pi^\nu(\vb y)} = 0$ \medskip
\\

$A_\mu(x) =$\\ \qquad $=\int \frac{\dd[3]{\vb p}}{(2\pi)^3} \frac{1}{\sqrt{2E_{\vb p}}}
\sum_{r = 0}^3 \qty(a_{\vb p}^r \varepsilon_\mu^r(p) e^{-ip \cdot x} + a_{\vb
p}^r^\dagger \varepsilon_\mu^r^*(p) e^{ip \cdot x})$ \\
$\varepsilon_\mu^r(p), r = 0, 1, 2, 3$ je ortonormirana baza, kjer je $r = 0$
timelike in ostali spacelike. \medskip \\

$\comm{a^r_{\vb p}}{ a^s_{\vb q}^\dagger} = -g_{rs} (2\pi)^3 \delta^{rs} \delta(\vb p - \vb q)$ \\
$\comm{a^r_{\vb p}}{ a^r_{\vb q}} = \comm{a^r_{\vb p}^\dagger}{ a^r_{\vb
q}^\dagger} = 0$ \medskip \\

Tipično vzamemo $k^\mu \propto (1, 0, 0, 1)$ in: \\
$\varepsilon^0 = (1, 0, 0, 0)$, $\varepsilon^1 = (0, 1, 0, 0)$, $\varepsilon^2 =
(0, 0, 1, 0)$, $\varepsilon^3 = (0, 0, 0, 1)$ \\
Zunanji fotoni lahko imajo le transverzalne polariz.\ ($r = 1, 2$). \medskip
\\

\subsubsection{Propagator}

$D_{\mu \nu}(x - y) = \ev{T\qty(A_\mu(x) A_\nu(y))}{0} = \int
\frac{\dd[4]{q}}{(2\pi)^4} \frac{-ig_{\mu\nu}}{q^2 + i \varepsilon} e^{-iq \cdot (x - y)}$, \\
kjer $\varepsilon \searrow 0$. \medskip \\

\subsubsection{Wardova identiteta}

$\mathcal{M} = \varepsilon_\mu^r^* \mathcal{M}^\mu$ \qquad $\implies$ \qquad
$k_\mu \mathcal{M}^\mu = 0$ \\
$\sum_{r = 1, 2} \abs{\mathcal M}^2 = \sum_{r = 1, 2} \varepsilon_\mu^r^*
\varepsilon_\nu^r \mathcal M^\mu \mathcal M^*^\nu = - g_{\mu\nu} \mathcal M^\mu
\mathcal M^*^\nu$ \medskip \\

\section{Interakcije}

$\mathcal L = \mathcal L_0 + \mathcal L_i$ \\
$\mathcal H = \mathcal H_0 + \mathcal H_i = \mathcal H_0 - \mathcal L_i$
\medskip \\

Za interakcije mora veljati:
\begin{itemize}[itemindent=-13pt]
	\itemsep-0.1em 
    \item kavalnost: interakcije so lokalne, \\
    \item Lorentzova invariantnost, \\
    \item renormalizablinost.
\end{itemize}
\medskip

Možne interakcije ($\mathcal L_i$): \\
\begin{itemize}[itemindent=-13pt]
	\itemsep-0.1em 
    \item $\mu \phi^3$ in $\lambda \phi^4$, \\
    \item $g \bar \psi \psi \phi$ (Yukawa), \\
    \item $e \bar \psi \gamma^\mu \psi A_\mu$ (kvantna elektrodinamika), \\
    \item $e A^\mu \phi \partial_\mu \phi$ in $e^2 \abs{\phi}^2 A^2$ (skalarna
        QED), \\
    \item $A^2 (\partial_\mu A^\mu)$ in $A^4$.
\end{itemize}
\medskip

\subsection{Interakcijska slika}

$\ket{\Omega}$ je osnovno stanje $H$ \\
$\phi_I(x)$ so op.\ definirani v prejšnjem poglavju (glede na $H_0$) \\
Iščemo $\phi(x)$, lastna stanja $H$. \medskip \\

$U(t, t_0) = e^{i H_0(t - t_0)} e^{-iH(t - t_0)}$ \\
$U(t, t_0)^\dagger = U(t_0, t)$ \\
$U(t_1, t_2)U(t_2, t_3) = U(t_1, t_3)$ \\
$\phi(t, \vb x) = U^\dagger(t, t_0) \phi_I(t, \vb x) U(t, t_0)$ \medskip \\

$i \pdv{U(t, t_0)}{t} = e^{iH_0(t - t_0)} H_i e^{-iH_0(t - t_0)} U(t, t_0) = H_I(t) U(t,
t_0)$, \\ z začetnim pogojem $U(t_0, t_0) = 1$ \\
$U(t, t_0) = 1 + (-i) \int_{t_0}^t \dd t_1 H_I(t_1) +$ \\ \hspace{42.5pt} $+ (-i)^2 \int_{t_0}^t \dd
t_1 \int_{t_0}^{t_1} \dd{t_2} H_I(t_1) H_I(t_2) + \cdots = $ \\ \hspace{27pt} $=
T\qty{\exp\qty[-i\int_{t_0}^t \dd t' H_I(t')]}$ \medskip \\

$\ket{\Omega} = \lim_{T \to \infty(1 - i \varepsilon)} \frac{U(t_0, -T)
\ket{0}}{e^{-i E_0 (t_0 - (- T))} \braket{\Omega}{0}}$, \\
kjer $E_0 = \ev{H}{\Omega}$ in $H_0 \ket{0} = 0$ \medskip \\

\subsection{Korelacijske funkcije}

$C_2(x, y) = \ev{T\qty{\phi(x) \phi(y)}}{\Omega} = $ \\ \hspace{20pt} $= \lim_{T \to
\infty(1 - i\varepsilon)} \frac{\ev{T\qty{\phi_I(x) \phi_I(y) \exp \qty[-i
\int_{-T}^T \dd t H_I(t)]}}{0}}{\ev{T\qty{\exp\qty[-i\int_{-T}^T \dd t
H_I(t)]}}{0}}$ \\ in analogno za $C_n$. \medskip \\

$C_n = \ev{T\qty{\phi(x_1) \cdots \phi(x_n)}}{\Omega} = $\\ \hspace{10pt} $=\qty(\text{vsota vseh
povezanih diagramov z $n$ zunanjimi točkami})$ \medskip \\

\subsection{Wickov izrek}

$N\qty{a_{j_1} \cdots a_{k_1}^\dagger \cdots a_{j_m} \cdots a_{k_m}^\dagger} = (\pm 1)^P 
a_{k_1}^\dagger \cdots a_{k_m}^\dagger a_{j_1} \cdots a_{j_m}$, \\
kjer je $P$ opravljena permutacija ($-$ za fermione). \medskip \\

Neničelne kontrakcije: \\
$\wick{\c \phi_I(x) \c \phi_I(y)} = D_F(x - y)$ \\
$\wick{\c \psi_I(x) \c {\overline \psi_I}(y)} = S_F(x - y)$ \\
$\wick{\c A_I_\mu(x) \c A_I_\nu(y)} = D_{\mu \nu}(x - y)$ \medskip \\

\textbf{Wickov izrek:} \\
$T\qty{\phi_I(x_1) \phi_I(x_2) \cdots \phi_I(x_m)} =$ \\ \qquad $= N\qty{\phi_I(x_1) \cdots
\phi_I(x_m) + \text{vse možne kontrakcije}}$ \\
Pri $\ev{T\qty{\cdots}}{0}$, so neničelni le polno kontrahirani členi. \\
Pri $\mel{\vb p_1}{T\qty{\cdots}}{\vb p_2}$ so neničelni le polno kontrahirani členi, kjer upoštevamo tudi kontrakcije
z zunanjimi stanji. \medskip \\

Neničelne kontrakcije z zunanjimi stanji: \\
$\wick{\c \phi_I(x) \ket{\c {\vb p}}_0} = e^{-ip\cdot x} \ket{0}$ \\
$\wick{_0\langle \c {\vb p} |\, \c \phi_I(x)} = e^{ip \cdot x} \bra{0}$ \\
$\wick{\c \psi_I(x) \ket{\c{\vb p, s, \text{fermion}}}_0} = e^{-ip \cdot x} u^s(p) \ket{0}$ \\
$\wick{\c{\overline \psi_I}(x) \ket{\c{\vb k, s, \text{antifermion}}}_0} = e^{-ik \cdot x}
\overline v^s(k) \ket{0}$ \\
$\wick{_0\langle \c {\vb p, s, \text{fermion}} |\, \c{\overline \psi_I}(x)} = e^{ip \cdot x}
\overline u^s(p) \bra{0}$ \\
$\wick{_0\langle \c {\vb k, s, \text{antifermion}} |\, \c \psi_I(x)} = e^{ik \cdot x}
v^s(k) \bra{0}$ \\
$\wick{\c A_\mu(x) \ket{\c \gamma(k, r)}_0} = \varepsilon_\mu^r(k) e^{-ik \cdot x}
\ket{0}$ \\
$\wick{_0\langle \c \gamma(k, r) |\, \c A_\mu(x)} = \varepsilon_\mu^r^*(k) e^{ik
\cdot x} \bra{0}$ \medskip \\

\subsection{Sipanje}

\subsubsection{$S$ matrika}

$\mel{\vb p_1 \vb p_2 \cdots}{S}{\vb k_A \vb k_B} = _\text{out}\braket{\vb p_1
\vb p_2 \cdots}{\vb k_A \vb k_B}_\text{in} = $ \\ \hspace{71.5pt} $=\lim_{T \to \infty}\mel{\vb p_1 \vb
p_2 \cdots}{e^{-iH(2T)}}{\vb k_A \vb k_B} $ \\
$S = 1 + iT$ \\
$\mel{\vb p_1 \vb p_2 \cdots}{iT}{\vb k_A \vb k_B} = (2\pi)^4 \delta(k_A + k_B -
\sum p_f) i\mathcal{M}(k_i \to p_f)$ \medskip \\

$\mel{\vb p_1 \cdots \vb p_n}{i T}{\vb k_\mathcal{A} \vb k_\mathcal{B}} \approx
\lim_{T \to \infty(1 - i \varepsilon)}$\\ \quad $\qty(_0\mel{\vb p_1 \cdots \vb
p_n}{T\qty{\exp\qty[-i \int_{-T}^T \dd{t} H_I(t)]}}{\vb k_\mathcal{A} \vb
k_\mathcal{B}}_0)_\parbox{35pt}{\tiny polno povezani, amputirani}$ \medskip \\

\subsubsection{Sipalni presek}

$\dd N_\text{sip} = \frac{\dd\sigma N_\mathcal{A} N_\mathcal{B}}{A}$ \medskip \\

Za ozka valovna paketa centrirana na $k_\mathcal{A}, k_\mathcal{B}$: \\
$\dd\sigma = \frac{1}{2E_\mathcal{A} 2E_\mathcal{B} \abs{v_\mathcal{A} -
v_\mathcal{B}}} \qty(\prod_f \frac{\dd[3]\vb p_f}{(2\pi)^3} \frac{1}{2E_f})
\abs{\mathcal M(k_\mathcal{A}, k_\mathcal{B} \to f)}^2$ \\ \hspace{22pt}$(2\pi)^4
\delta(k_\mathcal{A} + k_\mathcal{B} - \sum p_f)$ \medskip \\

Dva končna delca, v težiščnem sistemu: \\
$\qty(\dv{\sigma}{\Omega})_\text{CM} = \frac{1}{2 E_\mathcal{A} 2 E_\mathcal{B}
\abs{v_\mathcal{A} - v_\mathcal{B}}}\frac{\abs{\vb p_1}}{(2\pi)^2 4
        E_\text{CM}} \abs{\mathcal{M}}^2$ \\
Dodatno še vse štiri mase enake: \\
$\qty(\dv{\sigma}{\Omega})_\text{CM} = \frac{\abs{\mathcal{M}}^2}{64 \pi^2
E_\text{CM}^2 }$ \medskip \\

Nepolarizirano sipanje: \\
$\abs{\mathcal M_\text{nep.}}^2 = \frac{1}{N_s} \frac{1}{N_r} \sum_s \sum_r
\sum_{s'} \sum_{r'} \abs{\mathcal M(s, r \to s', r')}^2$ \medskip \\

\subsubsection{Feynmanova pravila za $\mathcal{M}$}

Propagatorji: \\
\vspace{-0.10cm}
\begin{itemize}[itemindent=-13pt]
	\itemsep-0.1em 
    \item skalarni \feynmandiagram[scale=0.5][horizontal=a to b] {
            a -- [scalar, edge label=\(q\)] b
        };\ : $\frac{i}{q^2 - m^2 + i \varepsilon}$
    \item fermionski \feynmandiagram[scale=0.5][horizontal=a to b] {
            a -- [fermion, edge label=\(p\)] b
        };\ : $\frac{i \qty(\slashed p + m)}{p^2 - m^2 + i \varepsilon}$
    \item fotonski \feynmandiagram[scale=0.5][horizontal=a to b] {
            a [particle=\(\mu\)] -- [photon, edge label=\(q\)] b
            [particle=\(\nu\)]
        };\ : $\frac{-ig_{\mu\nu}}{q^2 + i\varepsilon}$
\end{itemize}
\medskip

Vozlišča: \\
\vspace{-0.10cm}
\begin{itemize}[itemindent=-13pt]
	\itemsep-0.1em 
    \item $\phi^4$ \feynmandiagram[scale=0.2][horizontal=i1 to f2, edges={scalar}] {
            {i1, i2} -- c  -- {f1, f2},
        };\ : $-i\lambda$
    \item Yukawa \feynmandiagram[scale=0.3][horizontal=b to d] {
            a -- [fermion] b -- [scalar] d,
            b -- [fermion] c,
        };\ : $-ig \1$
    \item QED \feynmandiagram[scale=0.3][horizontal=b to d] {
            a -- [fermion] b -- [photon] d,
            b -- [fermion] c,
        };\ : $-ie \gamma^\mu$
\end{itemize}
\medskip

Začetne zunanje linije: \\
\vspace{-0.10cm}
\begin{itemize}[itemindent=-13pt]
	\itemsep-0.1em 
    \item skalar: $1$
    \item fermion: $u^s(p)$
    \item antifermion: $\bar v^s(p)$
    \item foton: $\varepsilon_\mu^r(p)$
\end{itemize}
\medskip

Končne zunanje linije: \\
\vspace{-0.10cm}
\begin{itemize}[itemindent=-13pt]
	\itemsep-0.1em 
    \item skalar: $1$
    \item fermion: $\bar u^s(p)$
    \item antifermion: $v^s(p)$
    \item foton: $\varepsilon_\mu^r^*(p)$
\end{itemize}
\medskip

Dodatno: \\
\vspace{-0.10cm}
\begin{itemize}[itemindent=-13pt]
	\itemsep-0.1em 
    \item Diracove matrike se množijo v nasprotni strani\\\hspace{-15pt} fermionskega toka,
    \item gibalna količina se ohranja v vseh vozliščih,
    \item integriraj po vseh nedoločenih momentih $\int
        \frac{\dd[4]{p}}{(2\pi)^4}$,
    \item množi s simetrijskim faktorjem in fermionskim predznakom.
\end{itemize}
\medskip

\section{Renormalizacija QED}

\subsection{Uporabna zveze}

\textbf{Feynmanova parametrizacija}: \\
$\frac{1}{AB} = \int_0^1 \dd{x} \frac{1}{\qty[x A + (1 - x)B]^2} = \int_0^1
\int_0^1
\dd{x} \dd{y} \frac{1}{\qty[x A + yB]^2} \delta(x + y - 1)$ \medskip \\

\textbf{Wickova rotacija}: \\
$l^0 = il^0_E$, \qquad $\vb l = \vb l_E$ \\
$l^2 = -l_E^2$ \qquad ($l_E^\mu$ leži v evklidskem prostoru) \\
$\int_{-\infty}^\infty \dd{l^0} f(l^0) = i \int_{-\infty}^\infty \dd{l_E^0} f(i
l_E^0)$, \\ če $f$ nima polov v 1.\ in 3.\ kvadrantu \medskip \\

\textbf{Pogosti integrali po Wickovi rotaciji}: \\
$\int \frac{d^d l_E}{(2 \pi)^d} \frac{1}{(l_E^2 + \Delta)^n} =
\frac{1}{(4\pi)^{d/2}} \frac{\Gamma(n - \frac{d}{2})}{\Gamma(n)} \Delta^{-n +
\frac{d}{2}}$ \\
$\int \frac{d^d l_E}{(2 \pi)^d} \frac{l_E^\mu}{(l_E^2 + \Delta)^n} = 0$ \\
$\int \frac{d^d l_E}{(2 \pi)^d} \frac{l_E^2}{(l_E^2 + \Delta)^n} =
\frac{1}{(4\pi)^{d/2}} \frac{d}{2} \frac{\Gamma(n - \frac{d}{2} - 1)}{\Gamma(n)} \Delta^{-n +
\frac{d}{2} + 1}$ \medskip \\

$\lim_{d \to 4} \frac{M^{4 - d} \Gamma(2 - \frac{d}{2})}{(4\pi)^{d/2} \Delta^{2
- d/2}} =$ \\ \hspace{20pt} $=\lim_{\varepsilon \to 0} \frac{1}{(4\pi)^2} \qty[\frac{2}{\varepsilon}
- \ln \frac{\Delta}{M^2} - \gamma + \ln(4\pi)]$ \\
$\gamma \approx 0.577$ \qquad (Euler-Mascheroni konstanta) \medskip \\

\subsection{Dimenzijska regularizacija}
\begin{itemize}[itemindent=-13pt]
	\itemsep-0.1em 
    \item Zapišemo integrale v $d$ dimenzijah, $\dd[4]{l} \to \dd[d]{l} M^{4 -
        d}$. \\
    \item Rešimo integrale za poljubno dimenzijo s pomočjo \\ \hspace{-13pt}Feynmanove
        parametrizacije in Wickove rotacije. \\
    \item Nastavimo $d = 4 - \varepsilon$ in naredimo limito $\varepsilon \to
        0$.
\end{itemize}
\medskip
Dimenzijska regularizacija ohrani umeritvene simetrije. \medskip \\

\subsubsection{Popravki do reda ene zanke}

\feynmandiagram[scale=0.5][layered layout, horizontal=b to c] {
    a [particle=\(\mu\)] -- [photon, momentum=\(q\)] b
    -- [fermion, half left, looseness=1.5] c
    -- [fermion, half left, looseness=1.5] b,
    c -- [photon, momentum=\(q\)] d [particle=\(\nu\)],
};\ $= i \Pi_2^{\mu\nu}(q) = i \Pi_2(q^2) \qty(q^2 g^{\mu\nu} - q^\mu q^\nu)$ \\
$\Delta = m^2 - x(1-x)q^2$ \\
$i \Pi_2(q^2) = -\frac{2\alpha}{\pi} \int_0^1 \dd{x} x(1-x)
\qty[\frac{2}{\varepsilon} - \ln \frac{\Delta}{M} - \gamma + \ln 4 \pi]$ \medskip \\

\feynmandiagram[scale=0.5][layered layout, horizontal=b to c] {
    a -- [fermion, momentum=\(p\)] b
    -- [fermion] c,
    b -- [photon, half left, looseness=1.5] c,
    c -- [fermion, momentum=\(p\)] d,
};\ $= -i \Sigma_2(p)$ \\
$\Delta = p^2x(x - 1) + m^2 (1 - x)$ \\
$\Sigma_2 =\frac{\alpha}{4\pi} \int_0^1 \dd{x} \qty[\frac{2}{\varepsilon}
- \ln \frac{\Delta}{M^2} - \gamma + \ln 4 \pi]$ \\ \hspace{53pt} $\qty[- (2 - \varepsilon)
(\slashed p x) + (4 - \varepsilon)m]$ \medskip

\feynmandiagram[scale=0.4][horizontal=e to f] {
    a -- [fermion, momentum'=\(p\)] c -- [fermion] e -- [fermion] d --
    [fermion, rmomentum'=\(p'\)] b,
    e -- [photon] f,
    c -- [photon] d,
};\ $=-ie \bar u(p')\delta\Gamma^\mu u(p)$ \\
Vozlišče drevesnega reda + popravek: \\
$-ie \gamma^\mu -ie \delta \Gamma^\mu = -ie\qty(\gamma^\mu F_1(q^2) + i
\frac{\sigma^{\mu\nu} q_\nu}{2m} F_2(q^2))$ \\
$\sigma^{\mu\nu} = \frac{i}{2} \comm{\gamma^\mu}{ \gamma^\nu}$ \\
$F_2(q^2)$ je končen. \\
$F_1(q^2) = 1 + \delta F_1(q^2)$, \quad $\delta F_1(q^2) \propto
\frac{1}{\varepsilon}$ \medksip \\

\subsection{Renormalizacija}

\subsubsection{Elektronski propagator}

$\mathrm{Prop}_e(p) = \frac{i}{\slashed p - m - \Sigma(p, m)} =
\frac{iZ_2}{\slashed p - m_R} + \mathcal{O}((\slashed p - m_R)^2) = \frac{i}{\Gamma_e^{(2)}(p)}$ \\
$Z_2 \approx \frac{1}{1 - \eval{\pdv{\Sigma_2}{\slashed p}}_{\slashed p = m_R}}$
\\
$\mathrm{Prop}_e^{(R)}(p) = \frac{i}{\slashed p - m_R} + \mathcal{O}((\slashed p -
m_R)^2) = \frac{i}{\Gamma_{e, R}^{(2)}(p)}$ \\
$\psi_R = \sqrt{Z_2}^{-1} \psi$, \qquad $\Gamma_{e, R}^{(2)}(p) = \sqrt{Z_2}^2
\Gamma_e^{(2)}(p)$ \\ \medskip 

Renormalizacijska pogoja (ekvivalentno zgornjemu): \\
$\Gamma_{e, R}^{(2)}(\slashed p = m_R) = 0$ \\
$\pdv{\Gamma_{e, R}^{(2)}}{\slashed p} \qty(\slashed p = m_R) = 1$ \medskip

$m_R = m + \Sigma(\slashed p = m_R, m)$ \\ \medskip

\subsubsection{Fotonski propagator}
$\mathrm{Prop}_\gamma(p) = \frac{-ig_{\mu\nu}}{q^2(1 - \Pi(q^2))} = \frac{-i
g_{\mu\nu} Z_3}{q^2 \qty(1 - \qty[\Pi_2(q^2) - \Pi_2(0)])} =
\frac{-ig_{\mu\nu}}{\Gamma_\gamma^{(2)}(q)}$ \\
$Z_3 \approx \frac{1}{1 - \Pi_2(0)}$ \\
$\mathrm{Prop}^{(R)}_\gamma(p) = \frac{-i
g_{\mu\nu}}{q^2 \qty(1 - \qty[\Pi_2(q^2) - \Pi_2(0)])} =
\frac{-ig_{\mu\nu}}{\Gamma_{\gamma, R}^{(2)}(q)}$ \\
$A_R^\mu = \sqrt{Z_3}^{-1} A^\mu$, \qquad $\Gamma_{\gamma, R}^{(2)}(p) = \sqrt{Z_3}^2
\Gamma_\gamma^{(2)}(p)$ \\ \medskip 

Renormalizacijska pogoja (ekvivalentno zgornjemu): \\
$\Gamma_{\gamma, R}^{(2)}(q^2 = 0) = 0$ \\
$\pdv{\Gamma_{\gamma, R}^{(2)}}{q^2}\qty(q^2 = 0) = 1$ \medskip

\subsubsection{Vozlišče}

$\Gamma^{(3), \mu}(q) = -ie\qty(\gamma^\mu F_1(q^2) + i
\frac{\sigma^{\mu\nu} q_\nu}{2m} F_2(q^2))$ \\
$\Gamma^{(3), \mu}_R(q) = \sqrt{Z_2}^2 \sqrt{Z_3}\Gamma^{(3), \mu}(q)$ \medskip \\

Renormalizacijski pogoj: \\
$\Gamma^{(3), \mu}_R(q = 0) = -i e_R \gamma^\mu$ \medskip \\

$Z_2 (1 + \delta F_1(0)) = 1$ \\
$e_R = \sqrt{Z_3} e$ \medskip \\

\subsection{Opazljive posledice}

Efektivna sklopitvena konstanta za fotonski propagator: \\
$\alpha_{R, \mathrm{ef}} = \frac{\alpha_R}{1 - \Pi_2(q^2) + \Pi_2(0)}$ \medskip
\\

Pri nizkih energijah: \\
$V(\vb r) = - \frac{\alpha_R}{r} - \frac{r \alpha_R^2}{15 m_R^2}
\delta(\vb r)$ \medskip \\

Anomalni magnetni moment: \\
$g = 2(1 + F_2(0)) = 2 (1 + \frac{\alpha_R}{2\pi})$ \medskip \\

\subsection{Renormalizacija s protičleni}

Renormalizirana polja uvedemo že v Lagrangianu: \\
$A^\mu = \sqrt{Z_3} A^\mu_R$, \qquad $\psi = \sqrt{Z_2} \psi_R$. \\
Nastavimo $Z_2 = 1 + \delta_2$, $Z_3 = 1 + \delta_3$ in: \\
$Z_2m = m_R + \delta_m$, \qquad $e = e_R Z_1 M^{\varepsilon/2}$, \qquad $Z_1 = 1
+ \delta_1$. \\
Izračunamo nova Feynmanova pravila ter upoštevamo renormalizacijske zahteve, kar
vodi do vrednosti $\delta_i$.
Opazljivi rezultati so enaki kot prej. \medskip \\

\section{LSZ zveza za skalarna polja}

$\mathrm{Prop}(q) \approx \frac{i Z}{q^2 - m_R^2 + i \varepsilon}$ \\
$\sqrt{Z} = \mel{\Omega}{\phi(x = 0)}{\vb q = 0}$ \medskip \\

$\prod_{i = 1}^n \int \dd[4]{x_i} e^{ip_i \cdot x_i} \prod_{j = 1}^m \int \dd[4]{y_i}
e^{-ik_j \cdot y_j}$ \\ \hspace{30pt} $\ev{T\qty{\phi(x_1) \cdots \phi(x_n) \phi(y_1) \cdots
\phi(y_m)}}{\Omega} = $ \\ \quad $= \prod_{i = 1}^n \frac{\sqrt{Z} i}{p_i^2 -
m_R^2 + i \varepsilon} \prod_{j = 1}^m \frac{\sqrt{Z} i}{k_j^2 - m_R^2 + i
\varepsilon} \mel{\vb p_1 \cdots \vb p_n}{S}{\vb k_1 \cdots \vb k_m}$ \medskip \\

\section{Feynmanov popotni integral}

Kvantna mehanika: $U(x_a, x_b; T) = \mel{x_a}{e^{-iHT}}{x_b} = \int
\mathcal{D}x(t) \, e^{iS[x(t)]}$ \medskip \\

Skalarno polje: \\
$\mel{\phi_b(\vb x)}{e^{-iHT}}{\phi_a(\vb x)} = \int \mathcal{D}\phi \,
\exp[i\int_0^T \dd[4]{x}\mathcal{L}]$ \\
$\ev{T\qty{\hat \phi(x_1) \hat \phi(x_2)}}{\Omega} = \lim_{T \to \infty(1 - i
\varepsilon)} \frac{\int \mathcal{D} \phi \, \phi(x_1) \phi(x_2) \exp[iS]}{\int
\mathcal{D} \phi \, e^{iS}}$ \medskip \\

\columnbreak
\vfill\null


\end{multicols}
\end{document}
