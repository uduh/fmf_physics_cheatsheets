% !TeX spellcheck = en_GB

\documentclass[10pt,landscape]{article}
\usepackage{multicol}
\usepackage{calc}
\usepackage{ifthen}
\usepackage[landscape]{geometry}
\usepackage{amsmath,amsthm,amsfonts,amssymb}
\usepackage{color,graphicx,overpic}
\usepackage{hyperref}
\usepackage{amsthm}
\usepackage{mathrsfs}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{icomma}
\usepackage{soul}
\usepackage{physics}
\usepackage{simpler-wick}

\pdfinfo{
	/Title (Useful formulas from advanced quantum mechanics)
	/Author (Urban Duh)
	/Subject (Advanced qunatum mechanics)}

% This sets page margins to .5 inch if using letter paper, and to 1 cm
% if using A4 paper. (This probably isn't strictly necessary.)
% If using another size paper, use default 1cm margins.
\ifthenelse{\lengthtest { \paperwidth = 11in}}
{ \geometry{top=.5in,left=.5in,right=.5in,bottom=.5in} }
{\ifthenelse{ \lengthtest{ \paperwidth = 297mm}}
	{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
	{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
}

% Turn off header and footer
\pagestyle{empty}

% Redefine section commands to use less space
\makeatletter
\renewcommand{\section}{\@startsection{section}{1}{0mm}%
	{-1ex plus -.5ex minus -.2ex}%
	{0.5ex plus .2ex}%x
	{\normalfont\large\bfseries}}
\renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
	{-1explus -.5ex minus -.2ex}%
	{0.5ex plus .2ex}%
	{\normalfont\normalsize\bfseries}}
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
	{-1ex plus -.5ex minus -.2ex}%
	{1ex plus .2ex}%
	{\normalfont\small\bfseries}}
\makeatother

% Don't print section numbers
\setcounter{secnumdepth}{0}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0pt plus 0.5ex}

% My Environments
\renewcommand{\H}{\mathcal{H}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\1}{\text{\usefont{U}{bbold}{m}{n}1}}
% -----------------------------------------------------------------------

\begin{document}
\raggedright
\footnotesize
\begin{multicols}{3}

\section{Composite systems}

\subsection{Mixed states}

For an orthonormal basis (ONB) $\qty{\ket{n}}$: $\tr X = \sum_n \ev{X}{n}$. \\
Trace is independent of the choice of the basis. \medskip \\

Density matrix for pure state $\ket{\psi}$: $\rho = \ket{\psi} \bra{\psi}$ \\
Density matrix for mixed state: $\rho = \sum_n p_n \ket{\psi_n} \bra{\psi_n}$, where $\sum_n p_n = 
1$, $p_n \geq 0$. \medskip \\

$\ev{A} = \tr(\rho A) = \tr(A \rho)$ \\
$\rho^\dagger = \rho$ \\
$\tr \rho = 1$ \\
$\rho \geq 0$ \qquad (positive semi-definite) \medskip \\

$\rho$ is pure $\iff$ $\tr(\rho^2) = 1$. Otherwise $\tr (\rho^2) < 1$. \medskip \\

\subsection{Two-component system}

$\H = \H_1 \otimes \H_2$ \medskip \\

If $\ket{\psi} = \ket{\varphi_1} \otimes \ket{\varphi_2}$, then $\ket{\psi}$ is separable. \\
Otherwise $\ket{\psi}$ is entangled. \medskip \\

$\H_{1, 2} \cong \mathbb{C}^2$, $\ket{\psi} = \sum_{ij} c_{ij} \ket{i} \otimes \ket{j}$ is separable 
$\iff$ $\det C = 0$. \medskip \\

\subsubsection{Partial trace}

For an ONB of $\H_i$ $\qty{\ket{n}_i}$: $\tr_i A = \sum_n 
{}_i\hspace{-1.5pt}\ev{A}{n}_i$. \medskip \\

$\rho_1 = \tr_2 \rho$, \qquad $\rho_2 = \tr_1 \rho$ \\
Eigenvalue spectrums of $\rho_1$ and $\rho_2$ are the same. \\
$\H_{1, 2} \cong \mathbb{C}^2$, $\rho$ is separable $\iff$ $\tr(\rho_1^2) = \tr(\rho_2^2) = 1$. 
\medskip \\

\subsubsection{Schmidt decomposition}

$d_i = \dim \H_i$ \\
$\ket{\psi} = \sum_{ij}c_{ij} \ket{i}_1 \otimes \ket{j}_2 = \sum_{k = 1}^{\min(d_1, d_2)} 
\sigma_k \ket{u_k}_1 \otimes \ket{v_k}_2$, where
$C = U \Sigma V^\dagger$ $\iff$ $c_{ij} = \sum_k \sigma_k \braket{i}{u_k} \braket{v_k}{j}$ is the 
SVD. \medskip \\

$C = U \Sigma V^\dagger$ $\implies$ $C^\dagger C = V \Sigma^2 V^\dagger$ (eigenvalue decomposition) 
\medskip \\


\section{Second quantization}

System of $N$ indistinguishable particles. \\
$\hat H(\vb r_1, s_1, \vb r_2, s_2, \dots, \vb r_N s_N) = \hat H(1, 2, \dots, N)$  \\
$\ket{\psi} \in \H_1^{\otimes N} = \H_N$ \medskip \\

\subsection{Wave function permutations}

$\hat P \ket{1}_1 \ket{2}_2 \cdots \ket{N}_N = \ket{1}_{P_1} \ket{2}_{P_2} \cdots \ket{N}_{P_N} 
=$\\ \hspace{73pt}$=\ket{P^{-1}_1}_1 \ket{P_2^{-1}}_2 \cdots \ket{P^{-1}_N}_N$ \\
$\hat P \sum \psi(x_1, x_2, \dots, x_N) \ket{x_1, x_2, \dots, x_N} = \sum \psi\qty(x_{P_1}, 
x_{P_2}, \dots, x_{P_N}) \ket{x_1, x_2, \dots, x_N}$ \medskip \\

\textbf{Postulate}: symmetry of $\hat H$, $\hat H \hat P = \hat P \hat H$. \medskip \\

$\hat H \ket{\psi} = E \ket{\psi}$ \quad $\implies$ \quad $\hat H (\hat P \ket{\psi}) = E (\hat 
P \ket{\psi})$, $\forall P \in S_N$ \\
$\braket{\varphi}{\psi} = \braket{\hat P \varphi}{\hat P \psi}$, $\forall P \in S_N$ and 
$\forall \ket{\psi}, \ket{\varphi} \in \H_N$ \\
$\hat P$ is unitary. \\
$\comm{\hat P}{\hat S} = 0$, $\forall P \in S_N$ $\implies$ $\mel{\hat P 
\psi_i}{\hat S}{\hat P \psi_j} = \mel{\psi_i}{\hat S}{\psi_j}$, $\forall \ket{\psi_i}, 
\ket{\psi_j} \in \H_N$ \medskip \\

All observables are symmetric operators. \\
Symmetry of a state does not change through time. \medskip \\

\subsubsection{Completely symmetric and antisymmetric states}

$\hat P \ket{\psi_S} = \ket{\psi_S}$ - completely symmetric, bosons \\
$\hat P \ket{\psi_A} = (-1)^P \ket{\psi_A}$ - completely antisymmetric, fermions \medskip \\

$\hat S_\pm = \frac{1}{\sqrt{N!}} \sum_{P \in S_N} (\pm 1)^P \hat P$ \\
$\hat P (\hat S_{\pm} \ket{\psi}) = (\pm 1)^P  \hat S_\pm \ket{\psi}$ \medskip \\

$\qty(\hat S_\pm)^2 = \sqrt{N!} \hat S_\pm$ \\
$\hat S_\pm^\dagger = \hat S_\pm$ \\
$\hat S_\pm \ket{j_1, \dots, j_N}$ form a basis of compl.\ (anti)symmetric states. \medskip \\

\subsection{Bosonic Fock space}

$\ket{\underline n} = \ket{n_1, n_2, \dots, n_L} = \frac{\hat S_+ \ket{j_1, j_2, \dots, 
j_N}}{\sqrt{n_1! n_2 ! \cdots n_L!}}$, where $n_i = \abs{\qty{j_k = i}}$ \\
$\braket{\underline n'}{\underline n} = \delta_{\underline n, \underline n'} = \delta_{n_1, 
n_1'} \delta_{n_2, n_2'} \cdots \delta_{n_L, n_L'}$ \\
$\1 = \sum_{\underline n} \ket{\underline n} \bra{\underline n}$ \medskip \\

$\F_N^+ = \hat S_+ \qty(\otimes_{k = 1}^N \H_k)$ \\
$\F_0^+ = \qty{c \ket{\emptyset}; c \in \mathbb{C}}$ \\
$\F^+ = \oplus_{N = 0}^L \F_N$ \qquad (bosonic Fock space) \\
$\ket{\underline n}$ form an ONB of $\F^+$ \medskip \\


\subsubsection{Creation and annihilation operators}

$\hat a_j^\dagger \ket{\dots, n_j, \dots} = \sqrt{n_j + 1} \ket{\dots, n_j + 1, \dots}$ \\
$\hat a_j \ket{\dots, n_j, \dots} = \sqrt{n_j} \ket{\dots, n_j - 1, \dots}$ \\
$\ket{n_1, n_2, \dots, n_l} = \frac{\qty(\hat a_1^\dagger)^{n_1} \qty(\hat a_2^\dagger)^{n_2} 
\cdots \qty(\hat a_L^\dagger)^{n_L}}{\sqrt{n_1! n_2! \cdots n_L!}}\ket{\emptyset}$ \medskip \\

Canonical commutation relations (CCR): \\
$\comm{\hat a_j}{ \hat a_l} = \comm{\hat a_j^\dagger}{ \hat a_l^\dagger} = 0$ \\
$\comm{\hat a_j}{ \hat a_l^\dagger} = \delta_{j, l} \1$ \medskip \\

\subsection{Fermionic Fock space}

$\hat S_- \ket{j_1, j_2, \dots j_n} = \frac{1}{\sqrt{N!}} \mqty|\ket{j_1}_1 & \ket{j_1}_2 & 
\dots &\ket{j_1}_N \\ \vdots & \vdots & \ddots &\vdots \\ \ket{j_N}_1 & \ket{j_N}_2 & \dots 
&\ket{j_N}_N|$ \\
$\hat S_- \ket{j_1, j_2, \dots j_n} = - \hat S_- \ket{j_2, j_1, \dots j_n}$ \\
$j_k = j_l$ for some $k \neq l$ $\implies$ $\hat S_- \ket{j_1, j_2, \dots j_n} = 0$ \\
$\psi_A(\vb x_1, \dots, \vb x_n) = \frac{1}{\sqrt{N!}} \mqty|\varphi_{j_1}(\vb x_1) & 
\varphi_{j_1}(\vb x_2) 
& \dots &\varphi_{j_1}(\vb x_N) \\ \vdots & \vdots & \ddots &\vdots \\ \varphi_{j_N}(\vb x_1) & 
\varphi_{j_N}(\vb x_2) 
& \dots &\varphi_{j_N}(\vb x_N)|$ \medskip \\

$\ket{\underline n} = \ket{n_1, \dots, n_L} = \hat S_- \ket{j_1, \dots, j_N}$, where $j_1 < j_2 
< \cdots < j_N$ and  $n_i = \abs{\qty{j_k = i}} \in \qty{0, 1}$ \\
$\braket{\underline n'}{\underline n} = \delta_{\underline n, \underline n'} = \delta_{n_1, 
	n_1'} \delta_{n_2, n_2'} \cdots \delta_{n_L, n_L'}$ \\
$\1 = \sum_{\underline n} \ket{\underline n} \bra{\underline n}$ \medskip \\

$\F_N^- = \hat S_- \qty(\otimes_{k = 1}^N \H_k)$ \\
$\F_0^- = \qty{c \ket{\emptyset}; c \in \mathbb{C}}$ \\
$\F^- = \oplus_{N = 0}^L \F_N$ \qquad (fermionic Fock space) \\
$\ket{\underline n}$ form an ONB of $\F^-$ \medskip \\

\subsubsection{Creation and annihilation operators}

$\hat a_j^\dagger \ket{\dots, n_j, \dots} = (1 - n_j) (-1)^{\sum_{l < j} n_l} \ket{\dots, n_j + 
1, \dots}$ \\
$\hat a_j \ket{\dots, n_j, \dots} = n_j (-1)^{\sum_{l < j} n_l} \ket{\dots, n_j - 1, \dots}$ \\
$\ket{n_1, n_2, \dots, n_L} = \qty(\hat a_1^\dagger)^{n_1} \qty(\hat a_2^\dagger)^{n_2} \cdots 
\qty(\hat a_L^\dagger)^{n_l} \ket{\emptyset}$ \medskip \\

Canonical anti-commutation relations (CAR): \\
$\acomm{\hat a_j}{ \hat a_l} = \acomm{\hat a_j^\dagger}{ \hat a_l^\dagger} = 0$ \\
$\acomm{\hat a_j^\dagger}{ \hat a_l} = \delta_{j, l} \1$ \medskip \\

\subsection{Single- and two-particle operators}

$\hat n_j \ket{\underline n} = \hat a_j^\dagger \hat a_j \ket{\underline n} = n_j 
\ket{\underline n}$ \\
$\comm{\hat n_j}{ \hat n_l} = 0$ \\
$\hat N = \sum_{j = 1}^\infty \hat n_j$ \medskip \\

$\sum_{k = 1}^N \ket{j}_k \bra{l}_k = \hat a_j^\dagger \hat a_l$ \\
$\hat t = \sum_{j, l} t_{jl} \sum_{k = 1}^N \ket{j}_k \bra{l}_k = \sum_{j, l} t_{jl} \hat 
a_j^\dagger \hat a_l$ \ \ (single-particle op.) \medskip \\

Two-particle operator: \\
$\hat F = \sum_{k, k' \neq k}^N f^{(2)}\qty(\vb x_k, \vb x_{k'})$ \\
$\hat F = \frac{1}{2} \sum_{j, j', l, l'} \mel{j, j'}{f^{(2)}}{l, l'} \sum_{k, k' \neq k}^N 
\ket{j}_k \ket{j'}_{k'} \bra{l}_k \bra{l'}_{k'} =$ \\ \hspace{6pt} $=\frac{1}{2} \sum_{j, j', l, 
	l'} 
\mel{j, j'}{f^{(2)}}{l, l'} \hat a_j^\dagger \hat a_{j'}^\dagger \hat a_{l'} \hat a_l$ \medskip 
\\

\subsection{Field operators}

Change of basis $\qty{\ket{j}} \to \qty{\ket{\lambda}}$:
$\ket{\lambda} = \sum_j \ket{j} \braket{j}{\lambda}$ \\
$\hat a_\lambda^\dagger = \sum_j \braket{j}{\lambda} a_j^\dagger$ \\
$\hat a_\lambda = \sum_j \braket{\lambda}{j} a_j$ \\
$\hat a_j, \hat a_j^\dagger$ satisfy CCR/CAR $\implies$ $\hat a_\lambda, \hat a_\lambda^\dagger$ 
satisfy CCR/CAR. \medskip \\

$\hat \Psi^\dagger(\vb x) = \sum_j \braket{j}{\vb x} \hat a_j^\dagger = \sum_j \varphi_j^*(\vb 
x) \hat a_j^\dagger$ \\
$\hat \Psi(\vb x) = \sum_j \braket{\vb x}{j} \hat a_j = \sum_j \varphi_j(\vb x) \hat a_j$ \\
$\hat \Psi(\vb x)$ do not depend on the choice of $\qty{\varphi_j(\vb x)}$. \\
$\comm{\hat \Psi(\vb x)}{ \hat \Psi(\vb x')}_\pm = \comm{\hat \Psi^\dagger(\vb x)}{ \hat 
\Psi^\dagger(\vb x')}_\pm = 0$ \\
$\comm{\hat \Psi(\vb x)}{ \hat \Psi^\dagger(\vb x')}_\pm = \delta(\vb x - \vb x') \1$ \medskip \\

$\hat n(\vb x) = \sum_k \delta(\vb x - \vb x_k) = \hat \Psi^\dagger(\vb x) \hat \Psi(\vb x)$ 
\quad (number density) \\
$\hat T = \frac{\hbar^2}{2m} \int \grad \hat \Psi^\dagger(\vb x) \grad \hat 
\Psi(\vb x) \dd[3]{\vb x}$ \quad (kinetic energy)\\
$\hat U = \int U(\vb x) \hat \Psi^\dagger(\vb x) \hat \Psi(\vb x) \dd[3]{\vb x} = \int U(\vb x) 
\hat n(\vb x) \dd[3]{\vb x}$ \quad 
\\ \qquad (single-particle potential) \\
$\hat V = \frac{1}{2} \iint \hat \Psi^\dagger(\vb x) \hat \Psi^\dagger(\vb x') V(\vb x, \vb x') 
\hat \Psi(\vb x') \hat \Psi(\vb x) \dd[3]{\vb x} \dd[3]{\vb x'} = $ \\ \hspace{6.5pt} $= 
\frac{1}{2} \iint 
V(\vb x, \vb x') \hat n(\vb x) \hat n(\vb x') \dd[3]{\vb x} \dd[3]{\vb x'}$ \\ \qquad  
(two-particle interaction), assuming $V(\vb x, \vb x) = 0$ \\
$\hat{\vb j}(\vb x) = \frac{\hbar^2}{2 im} \qty[\hat \Psi^\dagger(\vb x) \grad \hat \Psi(\vb x) 
- \grad \hat \Psi^\dagger (\vb x) \hat \Psi(\vb x)]$ \medskip \\

$\dv{\hat n(\vb x)}{t} = - \div{\hat{\vb j}(\vb x)}$ \\
$i \hbar \pdv{t} \hat \Psi(\vb x) = - \frac{\hbar^2}{2m} \laplacian{\hat \Psi(\vb x)} + 
\qty(U(\vb x) + \int V(\vb x, \vb x') \hat n(\vb x') \dd[3]{\vb x'}) \hat \Psi(\vb x)$ \medskip 
\\

\subsection{Momentum representation}

The system is confined in a box with length $L$ with periodic boundary conditions. \\
ONB: $\varphi_{\vb k}(\vb x) = \frac{1}{\sqrt{V}} e^{i \vb k \cdot \vb x}$, $\vb k = \frac{2 
\pi}{L} (n_1, n_2, n_3)$, $n_i \in \mathbb{Z}$ \\
Fourier transform of $f$: $f_{\vb k} = \int f(\vb x) e^{i \vb k \cdot \vb x} \dd[3]{\vb x}$ 
\medskip \\ 

$\hat T = \sum_{\vb k} \frac{\hbar^2 k^2}{2m} \hat n_{\vb k}$ \\
$\hat U = \frac{1}{V} \sum_{\vb k', \vb k} U_{\vb k' - \vb k} \hat a_{\vb k}^\dagger \hat 
a_{\vb k'}$ \\
$\hat V = \frac{1}{2V} \sum_{\vb k, \vb k', \vb p, \vb p'} V_{\vb p - \vb k} \hat a_{\vb 
k}^\dagger \hat a_{\vb k'}^\dagger \hat a_{\vb p'} \hat a_{\vb p} \delta_{\vb k + \vb k', \vb p 
+ \vb p'} =$ \\ \hspace{9pt}$=\frac{1}{2V} \sum_{\vb p, \vb p', \vb q} V_{\vb q} \hat a_{\vb p 
- \vb q}^\dagger \hat a_{\vb p' + \vb q}^\dagger \hat a_{\vb p'} \hat a_{\vb p}$, \quad when $V 
= V(\vb x_1 - \vb x_2)$ \medskip \\

$\mathrm{FT}(\hat n(\vb x)) = \tilde{\hat n}_{\vb k} = \sum_{\vb p} \hat a_{\vb p + \vb 
k}^\dagger \hat a_{\vb p}$ \medskip \\

\subsection{Inclusion of spin}

$\ket{\varphi_{j, \sigma}} = \ket{\varphi_j} \otimes \ket{\sigma}$ \\
We accordingly define $\hat a_{j, \sigma}$, $\hat a_{j, \sigma}^\dagger$. \\
$\comm{\hat a_{j, \sigma}}{ \hat a_{j', \sigma'}}_\pm = \comm{\hat a_{j, \sigma}^\dagger}{ \hat 
a_{j', \sigma'}^\dagger}_\pm = 0$ \\
$\comm{\hat a_{j, \sigma}}{ \hat a_{j', \sigma'}^\dagger}_\pm = \delta_{j, j'} \delta_{\sigma, 
\sigma'} \1$ \medskip \\

$\hat \Psi_\sigma^\dagger(\vb x) = \sum_j \varphi_j^*(\vb x) \hat a_{j, \sigma}^\dagger$ \\
$\hat \Psi_\sigma(\vb x) = \sum_j \varphi_j(\vb x) \hat a_{j, \sigma}$ \\
$\comm{\hat \Psi_\sigma(\vb x)}{ \hat \Psi_{\sigma'}(\vb x')}_\pm = \comm{\hat 
\Psi_\sigma^\dagger(\vb x)}{ \hat \Psi_{\sigma'}^\dagger(\vb x')}_\pm = 0$ \\
$\comm{\hat \Psi_\sigma(\vb x)}{ \hat \Psi_{\sigma'}^\dagger(\vb x')}_\pm = \delta(\vb x - \vb 
x') \delta_{\sigma, \sigma'} \1$ \medskip \\

For spin independent operators, an additional sum over $\sigma$ must be introduced. \medskip \\

\textbf{Spin-$\frac{\mathbf 1}{\mathbf 2}$ fermions}: \\
$\hat{\vb S} = \frac{\hbar}{2} \hat{\boldsymbol \sigma} = \frac{\hbar}{2} \sum_{j, \sigma, \tau} 
\hat a_{j, \sigma}^\dagger \hat{\boldsymbol \sigma}_{\sigma, \tau} \hat a_{j, \tau}$ \\
$\hat{\vb S}(\vb x) = \sum_{k = 1}^N \delta(\vb x - \vb x_k) \hat{\vb S}_k =\frac{\hbar}{2} 
\sum_{\sigma, \tau} \hat \Psi_{\sigma}^\dagger(\vb x) \hat{\boldsymbol \sigma}_{\sigma, \tau} 
\hat 
\Psi_\tau(\vb x)$ \\
$H_\mathrm{Zeeman} = - \frac{g \mu_B}{2} \sum_{j, \sigma, \tau} 
\hat a_{j, \sigma}^\dagger (\hat{\boldsymbol \sigma}_{\sigma, \tau} \cdot \vb B) \hat a_{j, 
\tau}$ \medskip \\

\subsection{Mean field approximation}

For $A$, $B$ quadratic in creation/annihilation operators: \\
$A = \ev{A} + \delta A$  \\
$B = \ev{B} + \delta B$ \\
$\implies$ $AB \approx \ev{A} B + A \ev{B} - \ev{A} \ev{B}$ \quad (first order in $\delta A$, 
$\delta B$) \medskip \\

\section{Wick's theorem}

$:a_{j_1} \cdots a_{k_1}^\dagger \cdots a_{j_m} \cdots a_{k_m}^\dagger: \ = (\pm 1)^P 
a_{k_1}^\dagger \cdots a_{k_m}^\dagger a_{j_1} \cdots a_{j_m}$, \\
where $P$ is the permutation performed ($-$ sign for fermions).  \\
$\wick{\c a \c b} = ab - :ab:$ \medskip \\

$b_1 \cdots b_{2m} = \ :b_1 \cdots b_{2m}: + \sum_{p = 1}^m \qty(\sum_{p \ \text{c.}} 
(\pm 1)^P :b_1 \wick{\c1 \cdot \c2 \cdot \c1 \cdot\c2\cdot} b_{2m}:)$, \\
where $P$ is the perm.\ needed to contract nearest neighbours. \medskip \\

$\ev{b_1 b_2 \cdots b_{2m}}{\emptyset} = \sum_{m \ \text{contractions}} (\pm 1)^P \wick{\c1 b_1 \c2 
	b_2 \c3 \cdot \c2 \cdot \c3 \cdot \c1 b_{2m}}$, \\
where $P$ is the perm.\ needed to contract nearest neighbours. \medskip \\


$\wick{\c a_i \c a_j} = \wick{\c a_i^\dagger \c a_j^\dagger} = \wick{\c a_i^\dagger \c a_j} = 0$ \\
$\wick{\c a_i \c a_j^\dagger} = \delta_{i, j}$ \medskip \\

\section{Canonical transformations}

\subsection{1D Fourier transform}

$N$ sites, $j = 1, \dots, N$ \\
$c_1 = c_{L + 1}$ \quad $\implies$ \quad $k = 0, \dots, N - 1$ \\
$c_j = \frac{1}{\sqrt{N}} \sum_k e^{i \frac{2 \pi}{N} k j} d_k$ \\
$d_k = \frac{1}{\sqrt{N}} \sum_j e^{-i \frac{2 \pi}{N} k j} c_j$ \\
$\braket{k}{p} = \frac{1}{N} \sum_j e^{i \frac{2 \pi}{N} (p - k) j} = \delta_{p-k, 0 
(\text{mod} N)}$ \medskip \\

\subsection{Bosons}

$\mqty(a_1 \\ a_2^\dagger) = Q \mqty(b_1 \\ b_2^\dagger)$ canonical if $Q^\dagger \mqty[1 & 0 
\\ 0 & -1] Q = \mqty[1 & 0 \\ 0 & -1]$ \medskip \\

$\mqty(a_1 \\ a_2) = Q \mqty(b_1 \\ b_2)$ canonical if $Q$ unitary \medskip \\

\subsection{Fermions}

$\mqty(a_1 \\ a_2^\dagger) = Q \mqty(b_1 \\ b_2)$ canonical if $Q$ unitary \medskip \\

\subsection{Spins}

\subsubsection{Wigner-Jordan transformation}

spin-$\frac{1}{2}$ particles $\leftrightarrow$ spinless fermions \\
$a_j = \qty(\prod_{l < j} \sigma_l^z) \sigma_j^-$ \\
$a_j^\dagger = \qty(\prod_{l < j} \sigma_l^z) \sigma_j^+$ \medskip \\

$\sigma_j^+ = e^{i \pi \sum_{l < j} n_l} a_j^\dagger = \qty(\prod_{l < j} (1 - 2n_l)) 
a_j^\dagger$ \\
$\sigma_j^- = e^{i \pi \sum_{l < j} n_l} a_j = \qty(\prod_{l < j} (1 - 2n_l)) a_j$ \\
$\sigma_j^z = 2 n_j - \1$ \medskip \\

$\sigma^\pm = \frac{1}{2} \qty(\sigma^x \pm i \sigma^y)$

\subsubsection{Holstein-Primakoff transformation}

spin-$S$ particle, $S \gg 1$ $\leftrightarrow$ spinless boson \\
$s^z = S - a^\dagger a$ \\
$s^+ = \sqrt{2 S - a^\dagger a} a \approx \sqrt{2S} a$ \\
$s^- = a^\dagger \sqrt{2 S - a^\dagger a} \approx \sqrt{2S} a^\dagger$ \medskip \\

$s^\pm = s_x \pm i s_y$ \medskip \\

\subsubsection{Schwinger bosons}

spin-$S$ particle $\leftrightarrow$ $2$ spinless bosons \\
$s^\gamma = \frac{1}{2} \sum_{\alpha, \beta = 1}^2 a_\alpha^\dagger \qty(\sigma^\gamma)_{\alpha 
\beta} a_\beta$ \\
$s^z = \frac{1}{2} \qty(a_1^\dagger a_1 - a_2^\dagger a_2)$ \\
$s^+ = a_1^\dagger a_2$ \\
$s^- = a_2^\dagger a_1$ \\
$\ket{n_1, n_2}$ states correspond to $S = \frac{1}{2} (n_1 + n_2)$. \medskip \\

\section{Spin-$\frac{\mathbf 1}{\mathbf2}$ fermions}

\subsection{Free fermions}

$H = \sum_{\vb k, \sigma} \frac{\hbar^2 k^2}{2m} a^\dagger_{\vb k, \sigma} a_{\vb k, \sigma}$ \\
Fermi sea, ground state: $\ket{\phi_0} = \prod_{\vb k, \abs{\vb k} < k_F} \prod_\sigma 
a^\dagger_{\vb k, 
\sigma} \ket{\emptyset}$ \\
$k_F^3 = 3 \pi^2 \frac{N}{V}$ \medskip \\

Hole operator: $b_{\vb k, \sigma} = a^\dagger_{- \vb k, \sigma}$ \\
Particle-hole operator: $c_{\vb k, \sigma} = \begin{cases}
	a_{\vb k, \sigma}; & \abs{\vb k} > k_F \\
	b_{\vb k, \sigma}; & \abs{\vb k} \leq k_F
\end{cases}$ \\
$b_{\vb k, \sigma}$ and $c_{\vb k, \sigma}$ obey CAR. \medskip \\


\subsubsection{Wick's theorem in Fermi sea}

$a_{\vb k, \sigma} = \vartheta(\abs{\vb k} - k_F) c_{\vb k, \sigma} + \vartheta(k_F - \abs{\vb 
k}) c_{-\vb k, 
\sigma}^\dagger$ \\
$c_{\vb k, \sigma} \ket{\phi_0} = 0$ $\implies$ we can use Wick's theorem on $c_{\vb 
k, \sigma}$ in $\ket{\phi_0}$ \medskip \\

$\ev{a_1 \cdots a_{2m}}{\phi_0} = \sum_{m \ \text{contractions}} (\pm 1)^P \ev{\wick{\c1 a_1 
\c2 a_2 \c3 \cdot \c2 \cdot \c3 \cdot \c1 a_{2m}}}{\phi_0}$ \\
Only non-zero ``contractions" (contracting $c_{\vb k, \sigma}$ by definition) of particle 
operators in $\ket{\phi_0}$: \\
\hspace{-2pt}$\ev{a_{\vb k, \sigma} a_{\vb k', \sigma'}^\dagger}{\phi_0} = \ev{\wick{ \c a_{\vb 
k, \sigma} \c 
a_{\vb k', \sigma'}^\dagger}}{\phi_0} = \delta_{\vb k, \vb k'} \delta_{\sigma, \sigma'} 
\vartheta(\abs{\vb k} - k_F)$ \\
\hspace{-2pt}$\ev{a_{\vb k, \sigma}^\dagger a_{\vb k', \sigma'}}{\phi_0} = \ev{\wick{ \c a_{\vb 
k, 
\sigma}^\dagger \c a_{\vb k', \sigma'}}}{\phi_0} = \delta_{\vb k, \vb k'} \delta_{\sigma, 
\sigma'} \vartheta(k_F - \abs{\vb k})$ \medskip \\

\subsubsection{Correlation functions}

$G_{\sigma, \sigma'}(\vb x, \vb x') = \ev{\Psi^\dagger_{\sigma'}(\vb x') \Psi_{\sigma}(\vb 
x)}{\phi_0} =$ \\ \quad $=\delta_{\sigma, \sigma'} \frac{3}{2} \rho \frac{1}{k_F^3 r^3} 
\qty(\sin\qty(k_F r) - k_F r \cos(k_F r))$, \qquad $r = \abs{\vb x - \vb x'}$ \\
$g_{\sigma, \sigma'}(\vb x, \vb x') = \frac{4}{\rho^2} \ev{\Psi_\sigma^\dagger(\vb x) 
\Psi_{\sigma'}^\dagger(\vb x') \Psi_{\sigma'}(\vb x') \Psi_\sigma(\vb x)}{\phi_0} = $ \\ 
\hspace{41pt} $= \begin{cases}
1; & \sigma \neq \sigma' \\
1 - \abs{\frac{G_{\sigma, \sigma}(\vb x, \vb x')}{\rho / 2}}^2; & \sigma = \sigma'
\end{cases}$  \medskip \\

\subsection{Hartree-Fock approximation}

Unknown (variable) orbitals $\ket{j} = \ket{\varphi_j, \sigma_j}$, $j = 1, \dots, L$ with operators 
$a_j$. \\
Ground state approximation for $N < L$: $\ket{\psi} = \prod_{j = 1}^N a_j \ket{\emptyset}$. \\
$E\qty[\qty{\varphi_j}] = \frac{\ev{H}{\psi}}{\braket{\psi}}$ \\
We want $\fdv{E\qty[\qty{\varphi_j}]}{\varphi_j} = 0$, $\forall j$. \medskip \\ 

For Coulomb potential $U$ and interaction $V$ (atoms): \\
$\varepsilon_j \varphi_j(\vb x) = \qty(- \frac{\hbar^2}{2m} \laplacian - \frac{Z e_0^2}{4 \pi 
\varepsilon_0 \abs{\vb x}}) \varphi_j(\vb x) +$\\ $\hspace{-10pt} \sum_{l = 1}^N \int \dd[3]{\vb 
x'} 
\frac{e_0^2}{4 \pi \varepsilon_0 \abs{\vb x - \vb x'}} \qty(\abs{\varphi_l(\vb x')}^2 \varphi_j(\vb 
x) - \delta_{\sigma_j, \sigma_l} \varphi_l^* (\vb x') \varphi_j(\vb x') \varphi_l(\vb x))$ \\
$\varepsilon_j$ are Lagrange multipliers for constraint $\braket{\varphi_j}{\varphi_j} = 1$
\medskip \\

\section{Bosons}

\subsection{Free bosons}

$H = \sum_{\vb k} \frac{\hbar^2 k^2}{2m} a^\dagger_{\vb k} a_{\vb k}$ \\
$g(\vb x, \vb x') = \frac{1}{\rho^2} \ev{\Psi^\dagger(\vb x) \Psi^\dagger(\vb x') \Psi(\vb x') 
\Psi(\vb x)}{\underline n} =$\\ \hspace{28pt} $=1 + \abs{\frac{1}{N} \sum_{\vb k} n_{\vb k} e^{i 
\vb k \cdot (\vb x - 
\vb x')}} - \frac{1}{N^2} \sum_{\vb k} n_{\vb k}(n_{\vb k} + 1)$ 

\subsection{Gross-Pitaevski equation}

Describes Bose-Einstein condensates near $T = 0$. \\
Ground state approximation: $\ket{\psi_0} = \frac{1}{\sqrt{N!}} \qty(a^\dagger_\varphi)^N 
\ket{\emptyset}$ \\
$\psi(\vb x) = \sqrt{N} \varphi(\vb x)$ $\implies$ $\braket{\psi} = N$ \\
$V = g \delta(\vb x - \vb x')$ \medskip 
\\
$-\frac{\hbar^2}{2m} \laplacian{\psi(\vb x)} + U(\vb x) \psi(\vb x) + g \abs{\psi(\vb x)}^2 
\psi(\vb x) = \varepsilon \psi(\vb x)$ \\
$\varepsilon$ is the Lagrange multiplier for constraint $\braket{\psi}{\psi} = N$
$-\frac{\hbar^2}{2m} \laplacian{\psi(\vb x, t)} + U(\vb x) \psi(\vb x, t) + g \abs{\psi(\vb x)}^2 
\psi(\vb x, t) = i \hbar \pdv{\psi(\vb x,tr)}{t}$ \medskip \\


\subsection{Bogoliubov theory}

Describes low $T$ dilute (weakly interacting) Bose gases. \\
$\ket{N_0, n_1, \dots}$, where $n_k \ll N_0$ \\
$\ket{N_0, \underline n} \approx \ket{N_0 + 1, \underline n} \approx \ket{N_0 - 1, \underline n}$ \\
$a_0 \approx \sqrt{N_0} \approx a_0^\dagger$ \medskip \\

$H = \sum_{\vb k} \frac{k^2}{2m} a_{\vb k}^\dagger a_{\vb k} + \frac{1}{2V} \sum_{\vb k, \vb p, \vb 
q} V_{\vb q} a^\dagger_{\vb k + \vb q} a^\dagger_{\vb p - \vb q} a_{\vb p} a_{\vb k} \approx$ \\ 
\hspace{8pt} $\approx \sum_{\vb k \neq 0} \frac{k^2}{2m} a_{\vb k}^\dagger a_{\vb k} + 
\frac{\rho 
N V_0}{2} +$ \\ \hspace{17pt} $+\frac{1}{2} \sum_{\vb k \neq 0} \rho V_{\vb k} \qty(2 a_{\vb 
k}^\dagger a_{\vb k} + a_{\vb k}^\dagger a^\dagger_{-\vb k} + a_{\vb k} a_{-\vb k})$ \medskip \\


$a_{\vb k} = u_{\vb k} \alpha_{\vb k} + v_{\vb k} \alpha_{- \vb k}^\dagger$ \\
$a^\dagger_{-\vb k} = u_{\vb k} \alpha_{-\vb k}^\dagger + v_{\vb k} \alpha_{\vb k}$ \\
This transformation is canonical if $u_{\vb k}^2 - v_{\vb k}^2 = 1$, $\forall \vb k$. \medskip \\

$H = E_0 + \sum_{\vb k \neq 0} \omega_{\vb k} \alpha_{\vb k}^\dagger \alpha_{\vb k}$ \\
$E_0 = \frac{N \rho V_0}{2} - \frac{1}{2} \sum_{\vb k \neq 0} \qty(\frac{k^2}{2m} + \rho V_{\vb k} 
- \omega_{\vb k})$ \\
$\omega_{\vb k} = \sqrt{\qty(\frac{k^2}{2m})^2 + \frac{k^2 \rho V_{\vb k}}{m}}$ \\
$u_{\vb k}^2 = \frac{\omega_{\vb k} + \frac{k^2}{2m} + \rho V_{\vb k}}{2 \omega_{\vb k}}$ \\
$v_{\vb k}^2 = \frac{-\omega_{\vb k} + \frac{k^2}{2m} + \rho V_{\vb k}}{2 \omega_{\vb k}}$ \\
$u_{\vb k} v_{\vb k} = - \frac{\rho V_{\vb k}}{2 \omega_{\vb k}}$ \medskip \\

$\alpha_{\vb k} \ket{\mathrm{GS}} = 0$ \quad $\forall \vb k$, \qquad \qquad $E_\mathrm{GS} = E_0$ \\
$N' = N - N_0 = \sum_{\vb k \neq 0} v_{\vb k}^2$ \medskip \\

\section{Time evolution}

\subsection{Time-independent Hamiltonians}

Propagator: $U(t, t_0) = e^{-\frac{i}{\hbar} (t - t_0) H}$ \\
Schr\" odinger's equation: $i \hbar \pdv{U(t, t_0)}{t} = H U(t, t_0)$ \\
$U(t_0, t_0) = \1$ \medskip \\

\subsubsection{Heisenberg's picture}

$\ev{A}(t) = \tr \rho A(t)$ \\
$A(t) = U^\dagger(t, t_0) A U(t, t_0)$ \\
$\pdv{A(t)}{t} = \frac{i}{\hbar} \comm{H}{A}(t)$ \medskip \\

\subsubsection{Schr\" odinger's picture}

$\ev{A}(t) = \tr \rho(t) A$ \\
$\rho(t) = U(t, t_0) A U^\dagger(t, t_0)$ \\
$\pdv{\rho(t)}{t} = - \frac{i}{\hbar} \comm{H}{\rho}(t)$ \medskip \\

\subsection{Correlation functions}

$\rho_\text{eq} = \frac{1}{Z} e^{- \beta H}$ \\
$\ev{A}_\text{eq} = \tr \rho_\text{eq} A$ \\
$\ev{A(t) B(t')}_\text{eq} = \ev{A(t + \tau) B(t' + \tau)}_\text{eq}$, \qquad for any $\tau \in 
\mathbb{R}$ \medskip \\

Green's functions: \\
$G_{AB}^>(t) = \ev{A(t) B}_\text{eq}$ \\
$G_{AB}^<(t) = \ev{B A(t)}_\text{eq}$ \\
Spectral functions: $G_{AB}^\lessgtr(\omega) = \int_{-\infty}^\infty G_{AB}(t) e^{i \omega t} 
\dd{t}$ \medskip \\

$G_{AB}^>(\omega) = G_{BA}^<(-\omega)$ \\
$G_{AB}^>(\omega) = e^{\beta \hbar \omega} G_{AB}^<(\omega)$ \\
$G_{AB}^>(t) = G_{AB}^<(t - i \beta \hbar \omega)$ \\
$G_{AA^\dagger}(\omega) \in \mathbb{R}$ \medskip \\

\subsection{Time-dependent Hamiltonians}

\subsubsection{Interaction picture}

$H(t) = H_0 + H'(t)$ \\
$U(t, t_0) = U_0(t, t_0) U'(t, t_0)$ \\
$U_0(t, t_0) = e^{-\frac{i}{\hbar} (t - t_0) H_0}$ \medskip \\

Operator in the interaction picture: $A_I(t) = U_0(t, t_0) A U_0(t, t_0)$ \\
$A(t) = U'^\dagger(t, t_0) A_I(t) U'(t, t_0)$ \\
$i \hbar \pdv{U'(t, t_0)}{t} = H_I'(t) U'(t, t_0)$ \\
$U'(t_0, t_0) = \1$ \medskip \\

\textbf{Tomonaga-Schwinger equation}:\\ 
$U'(t, t_0) = \1 - \frac{i}{\hbar} \int_{t_0}^t H_I'(t) U'(t, t_0) 
\dd{t}$ \medskip \\

\textbf{Dyson's expansion}: \\
$U'(t, t_0) = \sum_{p = 0}^\infty U_p'(t, t_0)$ \\
$U_0(t, t_0) = \1$ \\
$U_p(t, t_0) = - \frac{i}{\hbar} \int_{t_0}^t H_I'(t)U_{p - 1}(t, t_0) \dd{t}$ \medskip \\

\textbf{Time ordered exponential}: \\
$\hat \tau(A_I(t) B_I(t')) = \begin{cases}
	A_I(t) B_I(t'); & t \geq t' \\
	B_I(t') A_I(t); & t \leq t'
\end{cases}$ \\
$U'(t, t_0) = \hat \tau e^{- \frac{i}{\hbar} \int_{t_0}^t H_I'(t) \dd{t}}$ \medskip \\

\subsubsection{Linear response}

$H'(t) = -F(t) B$, \quad $F(t) \in \mathbb{R}$, \quad $F(t < t_0) = 0$ \\
We are doing 1st order Dyson's expansion, $\rho = \rho_\text{eq}$. \medskip \\

$\ev{A(t)}_\text{eq} - \ev{A(t_0)}_\text{eq} = \frac{i}{\hbar} \int_{t_0}^t 
\ev{\comm{A_I(t)}{B_I(t')}}_\text{eq} F(t') \dd{t'} =$\\ \hspace{78pt}$=\int_{-\infty}^\infty 
\chi_{AB}(t - t') F(t') \dd{t'}$ \\
$\chi_{AB}(t) = \frac{i}{\hbar} \vartheta(t) \ev{\comm{A_I(t)}{B_I}}_\text{eq} =$ \\ 
\hspace{26.5pt} $= \frac{i}{\hbar} \vartheta(t) \qty(G_{A_I B_I}^>(t) - G_{A_I B_I}^<(t))$ 
\medskip 
\\

$\chi_{AB}(\omega) = \int_{-\infty}^\infty \chi_{AB}(t) e^{i \omega t} \dd{t}$ \\
Kramers-Kronig: $\chi_{AB}(\omega) = \frac{1}{\pi i} \mathcal{P} \int_{-\infty}^\infty 
\frac{\chi_{AB}(\omega')}{\omega' - \omega} \dd{\omega'}$ \medskip \\

$\chi_{AB}''(t) = \frac{1}{2\hbar} \ev{\comm{A_I(t)}{B_I}}_\text{eq}$ \\
$\chi_{AB}'(\omega) = \frac{1}{\pi} \mathcal{P} \int_{-\infty}^\infty 
\frac{\chi_{AB}''(\omega)}{\omega' - \omega} \dd{\omega'}$ \\
$\chi_{AB}(\omega) = \chi_{AB}'(\omega) + i \chi_{AB}''(\omega)$ \medskip \\

$\comm{H}{\mathcal{T}} = 0$ and $\varepsilon_A \varepsilon_B = 1$ $\implies$ $\chi_{AB}''(\omega) 
\in \mathbb{R}$ \\
$\comm{H}{\mathcal{T}} = 0$ and $\varepsilon_A \varepsilon_B = -1$ $\implies$ $\chi_{AB}''(\omega) 
\in i\mathbb{R}$ \medskip \\

Fluctuation-dissipation theorem: \\
$\chi_{AB}''(\omega) = G_{A_I B_I}^>(\omega) \frac{1}{2 \hbar} \qty(1 - e^{- \beta \hbar \omega})$ 
\medskip \\

\subsection{Time reversal operator}

Time reversal symmetry: $\mathcal{T} A \mathcal{T}^{-1} = \varepsilon_A A$, \qquad $\varepsilon_A = 
\pm 1$ \\
$\varepsilon_{\vb x} = 1, \varepsilon_{\vb p} = -1, \varepsilon_{\sigma^\alpha} = -1$ \medskip \\

$\comm{H}{\mathcal{T}} = 0$ $\implies$ $\mathcal{T} U(t) \mathcal{T}^{-1} = U(-t)$ \\
\hspace{37 pt} $\implies$ $\mathcal{T} A(t) \mathcal{T}^{-1} = \varepsilon_A A(-t)$ \medskip \\

Anti-linearity: $\mathcal{T}\qty(\alpha \ket{\psi} + \beta \ket{\varphi}) = \alpha^* \ket{\psi} + 
\beta^* \ket{\varphi}$ \\
Anti-unitarity: $\braket{\psi}{\varphi} = \braket{\mathcal{T} \psi}{\mathcal{T} \varphi}^*$ \\
$\mathcal{T}^2 = \pm 1$ \medskip \\

Anti-unitarity $\implies$ $\mathcal{T} = KW$, $W$ unitary, $K$ conjugation. \\
Anti-unitarity $\implies$ anti-linearity. \medskip \\

$x$ representation: $\mathcal{T} = K$ \\
$x$ representation and spin $\frac{1}{2}$: $\mathcal{T} = i \sigma^y K$ \medskip \\

$\comm{H}{\mathcal{T}} = 0$, $\mathcal{T}^2 = 1$ $\implies$ $\exists \qty{\ket{n}}$: $H_{nm}$ is 
real and symmetric. \\
$\comm{H}{\mathcal{T}} = 0$, $\mathcal{T}^2 = -1$ $\implies$ all energy levels are twice degenerate 
(Kramers degeneracy). \medskip \\


\end{multicols}
\end{document}
