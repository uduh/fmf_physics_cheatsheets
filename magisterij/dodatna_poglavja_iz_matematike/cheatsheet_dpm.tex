% !TeX spellcheck = en_GB

\documentclass[10pt,landscape]{article}
\usepackage{multicol}
\usepackage{calc}
\usepackage{ifthen}
\usepackage[landscape]{geometry}
\usepackage{amsmath,amsthm,amsfonts,amssymb}
\usepackage{color,graphicx,overpic}
\usepackage{hyperref}
\usepackage{amsthm}
\usepackage{mathrsfs}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{icomma}
\usepackage{soul}
\usepackage{physics}

\pdfinfo{
	/Title (Useful formulas from advanced chapters in mathematics for physicists)
	/Author (Urban Duh)
	/Subject (Groups, manifolds)}

% This sets page margins to .5 inch if using letter paper, and to 1 cm
% if using A4 paper. (This probably isn't strictly necessary.)
% If using another size paper, use default 1cm margins.
\ifthenelse{\lengthtest { \paperwidth = 11in}}
{ \geometry{top=.5in,left=.5in,right=.5in,bottom=.5in} }
{\ifthenelse{ \lengthtest{ \paperwidth = 297mm}}
	{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
	{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
}

% Turn off header and footer
\pagestyle{empty}

% Redefine section commands to use less space
\makeatletter
\renewcommand{\section}{\@startsection{section}{1}{0mm}%
	{-1ex plus -.5ex minus -.2ex}%
	{0.5ex plus .2ex}%x
	{\normalfont\large\bfseries}}
\renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
	{-1explus -.5ex minus -.2ex}%
	{0.5ex plus .2ex}%
	{\normalfont\normalsize\bfseries}}
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
	{-1ex plus -.5ex minus -.2ex}%
	{1ex plus .2ex}%
	{\normalfont\small\bfseries}}
\makeatother

% Don't print section numbers
\setcounter{secnumdepth}{0}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0pt plus 0.5ex}

%My Environments
\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\im}{\mathrm{im}}
\newcommand*\sq{\mathbin{\vcenter{\hbox{\rule{.5ex}{.5ex}}}}}
\newcommand{\GL}{\mathrm{GL}}
\newcommand{\sprod}[1]{\langle#1\rangle}

\makeatletter
\newcommand{\os}{\mathbin{\mathpalette\make@circled s}}
\newcommand{\make@circled}[2]{%
	\ooalign{$\m@th#1\smallbigcirc{#1}$\cr\hidewidth$\m@th#1#2$\hidewidth\cr}%
}
\newcommand{\smallbigcirc}[1]{%
	\vcenter{\hbox{\scalebox{0.77778}{$\m@th#1\bigcirc$}}}%
}
\makeatother
% -----------------------------------------------------------------------

\begin{document}
\raggedright
\footnotesize
\begin{multicols}{3}
	
\section{Groups}

\textbf{Group} is a set $G$ equipped with a binary operation $\circ: G \times G \to G$, where:
\vspace{-0.10cm}
\begin{enumerate}[itemindent=-13pt]
	\itemsep-0.1em 
	\item $g \circ (h \circ f) = (g \circ h) \circ f$, \quad $\forall g, h, f \in G$ 
	\quad (associativity), \\
	\item $\exists e \in G$: $e \circ g = g \circ e = g$, \quad $\forall g \in G$ \quad 
	(identity element), \\
	\item $\forall g \in G \ \exists g^{-1} \in G$: $g \circ g^{-1} = g^{-1} \circ g = e$ 
	\quad (inverse el.).
\end{enumerate}
\vspace{-0.11cm}
\medskip

A group $(G, \circ)$ where $\forall g_{1, 2} \in G$: $g_1 \circ g_2 = g_2 \circ g_1$, is a 
\textbf{commutative} or an \textbf{abelian group}.  \medskip \\

Identity and inverse elements are unique. \medskip \\

\subsection{Subgroups}
	
$(G, \circ)$ a group, $H \subset G$. $(H, \circ)$ is a \textbf{subgroup} of $G$ ($H \leq G$) if:
\vspace{-0.10cm}
\begin{enumerate}[itemindent=-13pt]
	\itemsep-0.1em 
	\item $\forall h_1, h_2 \in H$: $h_1 \circ h_2 \in H$, \\
	\item $\forall h \in H$: $h^{-1} \in H$.
\end{enumerate}
\vspace{-0.11cm}
\medskip

Subgroups always contain identity elements. \medskip \\

$H \leq G$ $\iff$ ($h_1, h_2 \in H$ $\implies$ $h_1 \circ h_2^{-1} \in H$) \medskip \\

$H \leq G$, $G$ finite $\implies$ $\abs{H}$ divides $\abs{G}$ \medskip \\

\subsection{Homomorphisms}

$(G, \circ), (H, \sq)$ groups and $f: G \to H$. If 
$\forall (g_1, g_2) \in G \times G$: $f(g_1 \circ g_2) = f(g_1) \sq f(g_2)$, $f$ is a 
\textbf{group homomorphism}. \smallskip \\

$f(e_G) = e_H$ \\
$f(g^{-1}) = \qty(f(g))^{-1}$ \medskip \\

\textbf{Isomorphism} is a bijective homomorphism. \medskip \\

\textbf{Kernel}: $\ker f = \qty{g \in G; f(g) = e_H}$ \\
\textbf{Image}: $\im f = \qty{h \in H; \exists g \in G: f(g) = h}$ \smallskip \\

$\ker f \leq G$ \\
$\im f \leq H$ \medskip \\

$f$ injective $\iff$ $\ker f = \qty{e}$ \medskip \\

\subsection{Cosets}

$(G, \circ)$, $(H, \circ)$ groups and $H \leq G$: \\
\textbf{Left coset}: $g \circ H = \qty{g \circ h \in G; h \in H}$ \\
\textbf{Right coset}: $H \circ g = \qty{h \circ g \in G; h \in H}$ \medskip \\

For $g_{1, 2} \in G$, $g_1 \neq g_2$: either $g_1 H = g_2 H$ or $g_1 H \cap g_2H = \emptyset$. 
\smallskip \\
$G = \amalg_{g \in G} gH = \amalg_{g \in G} Hg$ \medskip \\

\textbf{Factor set}: $G / H = \qty{gH; g \in G}$ \medskip \\

$H \leq G$ is a \textbf{normal subgroup} ($H \triangleleft G$) of $G$ if $gH = Hg$ $\forall g 
\in G$ or equivalently $H = gHg^{-1}$ $\forall g \in G$. \medskip \\

$H \triangleleft G$ $\implies$ $(G / H, \sq)$ is a group, $(g_1 H) \sq (g_2 H) = (g_1 g_2)H$. 
\medskip \\ 

$f: G \to H$ a homomorphism $\implies$ $\ker f \triangleleft G$. \\
Every normal subgroup is the kernel of some homomorphism. \medskip \\

$Z(G) = \qty{z \in G; gz = zg, \forall g \in G} \triangleleft G$ \medskip \\

\section{Group actions}

$X$ a set, $G$ a group. \textbf{Left group action} of $G$ on $X$ is a map $\mathcal{A}: G \times 
X \to X, (g, x) \mapsto \mathcal{A}(g, x) = gx$, such that:
\vspace{-0.10cm}
\begin{enumerate}[itemindent=-13pt]
	\itemsep-0.1em 
	\item $(g_1 g_2)x = g_1(g_2x)$ $\iff$ $\mathcal{A}(g_1 g_2, x) = \mathcal{A}(g_1, 
	\mathcal{A}(g_2, x))$, \\
	\item $ex = x, \forall x \in X$. \\
\end{enumerate}
\vspace{-0.11cm}
\medskip

Alternatively, $A: G \to \mathrm{Aut}(X), g \mapsto \mathcal{A}(g, \bullet)$. \\
$A$ is a homomorphism. \medskip \\

\textbf{Orbit}: $\mathcal{O}_x = \qty{gx; g \in G}$ \\
\textbf{Set of all orbits}: $G \backslash X = \qty{\mathcal{O}_x; x \in X}$ \\
\textbf{Set of all fixed points}: $X^g = \qty{x \in X; gx = x}$ \medskip \\

An action is \textbf{transitive} if $\forall x \in X: \mathcal{O}_x = X$. \\
An action is \textbf{effective} (\textbf{faithful}) if: $\mathcal{A}(g, \bullet) = I$ $\implies$ 
$g = e$. \\
An action is \textbf{free} if: $g x_0 = x_0$ $\implies$ $g = e$. \medskip \\

\textbf{Burnside f.}: $G$ and $X$ finite $\implies$ $\abs{G \backslash X} = 
\frac{1}{\abs{G}} \sum_{g \in G} \abs{X^g}$. \medskip \\

\section{Group representations}

$G$ group, $V$ vector space. \textbf{Group representation} is a homomorphism $\rho: G \to 
\GL(V)$. \medskip \\

Representations $\rho_1: G \to \GL(V_1)$, $\rho_2: G \to \GL(V_2)$ are \textbf{equivalent} if 
$\exists$isomorphism $\tau: V_1 \to V_2$, such that $\rho_2(g)(v) = \qty(\tau \circ \rho_1(g) 
\circ \tau^{-1})(v)$, $\forall v \in V_2$, $\forall g \in G$. \medskip \\

\textbf{Action associated with a rep.}: $\mathcal{A}(g, v) = \rho(g)(v)$. \medskip \\

When a complex $V$ is equipped with a basis, representations map to $\GL(n,
\C) = \qty{A \in \C^{n \times n}; \det A \neq 0}$. \medskip \\

\subsection{Subrepresentations}

Subspace $W \subseteq V$ yields a \textbf{subrepresentation} $\rho^W: G \to \GL(W)$ of $\rho: G 
\to \GL(V)$ if $W$ is an invariant subspace of the action associated with $\rho$: $\forall g \in 
G, \forall w \in W: \rho(g)w \in W$. \medskip \\

$\rho$ rep.\ of finite $G$ and $W$ its subrep.: $\exists$subrep.\ $W^\circ$ of $\rho$, such 
that $V = W \oplus W^\circ$. \medskip \\

$\rho$ rep.\ of finite $G$: $\exists$ basis of $V$, where $\rho(g)$ are unitary $\forall g 
\in G$. \medskip \\

A rep.\ is \textbf{reducible} if there exists a non-trivial subrep. \\
A rep.\ is \textbf{irreducible} if it has no non-trivial subreps. \medskip \\

\subsection{Direct sums}

Two reps.\ $\rho_i: G \to \GL(V_i)$, the \textbf{direct sum} is given by $\rho_1 \oplus \rho_2: 
G \to \GL(V_1 \oplus V_2)$, $\qty(\rho_1 \oplus \rho_2)(g)(v_1, v_2) = \qty(\rho_1(g)(v_1), 
\rho_2(g)(v_2))$. \medskip \\

$G$ is compact (finite) $\implies$ every rep.\ is a direct sum of irreducible reps. \medskip \\

Two reps.\ $\rho_i: G \to \GL(V_i)$, linear map $f:V_1 \to V_2$ is \textbf{equivariant} if $f \circ 
\rho_1(g) = \rho_2(g) \circ f$, $\forall g \in G$. \medskip \\

\textbf{Shur's lemma}: If $\rho_i$ irreducible and $f$ is equivariant either:
\vspace{-0.10cm}
\begin{enumerate}[itemindent=-13pt]
	\itemsep-0.1em 
	\item $f$ is an isomorphism and $\rho_1, \rho_2$ are equivalent or \\
	\item $f(v_1) = 0, \forall v_1 \in V_1$.
\end{enumerate}
\vspace{-0.11cm}
If 1 ocurs and if $\rho_1 = \rho_2$, $V_1 = V_2$ $\implies$ $f = \lambda I$, $\lambda \in \C$. 
\medskip \\

\subsection{Tensor products}

$V_1$ and $V_2$ finite dimensional vector space. Vector space $W$ is a \textbf{tensor product} of 
$V_1$ onto $V_2$ if there exists a map $\otimes: V_1 \oplus V_2 \to W, (v_1, v_2) \mapsto w$, such 
that 
\vspace{-0.10cm}
\begin{enumerate}[itemindent=-13pt]
	\itemsep-0.1em 
	\item $\otimes$ is bilinear, \\
	\item $\qty{e_i}_{i = 1}^n$ basis of $V_1$, $\qty{f_i}_{i = 1}^m$ basis of $V_2$ $\implies$ 
	$\qty{e_i \otimes f_j}_{i, j}$ \\ \hspace{-15pt} basis of $W$. \\
\end{enumerate}
\vspace{-0.11cm}
\medskip

In canonical basis, we typically define $v_1 \otimes v_2 = [{v_1}_i {v_2}_j]_{i, j}$. \medskip \\

Two reps.\ $\rho_i: G \to \GL(V_i)$, $G$ finite. The \textbf{tensor product} is $\rho_1 
\otimes \rho_2: G \to \GL(V_1 \otimes V_2)$, $(\rho_1 \otimes \rho_2)(g) = \rho_1(g) \otimes 
\rho_2(g)$. \medskip \\

$A, B$ matrices: $A \otimes B = \mqty[A_{11} B & A_{12} B &  \cdots & A_{1n} B \\ \vdots & \vdots & 
\ddots & \vdots \\ A_{n1} B & A_{n2} B & \cdots & A_{nn} B]$ \medskip \\

\subsection{Characters}

The \textbf{character} of a rep.\ $\rho: G \to \GL(n; \C)$ is $\chi(g) = \tr \rho(g)$. \medskip \\

Properties:
\vspace{-0.10cm}
\begin{enumerate}[itemindent=-13pt]
	\itemsep-0.1em 
	\item $\chi(e) = n$, \\
	\item $\chi(g^{-1}) = \overline{\chi(g)}$, if $G$ is finite or compact, \\
	\item $\chi(hgh^{-1}) = \chi(g)$.
\end{enumerate}
\vspace{-0.11cm}
\medskip

$\rho_i$ representations of $G$ with characters $\chi_i$:
\vspace{-0.10cm}
\begin{enumerate}[itemindent=-13pt]
	\itemsep-0.1em 
	\item $\chi^\oplus$ character of $\rho_1 \oplus \rho_2$: $\chi^\oplus = \chi_1 + \chi_2$, \\
	\item $\chi^\otimes$ character of $\rho_1 \otimes \rho_2$: $\chi^\otimes = \chi_1 \cdot \chi_2$.
\end{enumerate}
\vspace{-0.11cm}
\medskip

\subsection{Representative functions}

$\rho: G \to \GL(n; \C)$ rep., $\rho(g) = [r_{ij}(g)]_{ij}$. $r_{ij}: G \to \C$ are 
\textbf{representative functions}. \medskip \\

For $f_1, f_2 \in C(G)$ define: $\sprod{f_1, f_2} = \frac{1}{\abs{G}} \sum_{g \in G} f_1(g) 
f_2(g^{-1})$. \medskip \\

$r^1_{ij}, r^2_{ij}$ given by two irreducible reps.\ $\rho_{1, 2}$:
\vspace{-0.10cm}
\begin{enumerate}[itemindent=-13pt]
	\itemsep-0.1em 
	\item $\rho_1$ not equivalent to $\rho_2$ $\implies$ $\sprod{r_{ij}^1, r_{lk}^2} = 0$, $\forall 
	i, j, k, l$, \\
	\item $V_1 = V_2$, $\rho_1 = \rho_2$ $\implies$
		  $\sprod{r_{ij}, r_{kl}} = \frac{1}{\dim V_1} \delta_{i, l} \delta_{j,k}$.
\end{enumerate}
\vspace{-0.11cm}
\medskip

\subsection{Character orthogonality relations}

For $\varphi, \psi \in C(G)$ define $(\varphi, \psi) = \frac{1}{\abs{G}} \sum_{g \in G} \varphi(g) 
\overline{\psi(g)}$. \\
For $\tilde \psi(g) = \overline{\psi(g^{-1})}$: $(\varphi, \psi) = \sprod{\varphi, \tilde \psi}$. \\
For characters of finite $G$: $(\chi_1, \chi_2) = \sprod{\chi_1, \chi_2}$. \medskip \\

Char.\ $\chi$ of irreducible rep.\ $\implies$ $(\chi, \chi) = 1$. \\
$\chi_{1, 2}$ chars.\ of two non-isomorphic irr.\ reps.\ $\implies$ $(\chi_1, \chi_2) = 
0$. \medskip \\

$\rho: G \to \GL(V)$ rep.\, $\rho = W_1 \oplus W_2 \oplus \cdots \oplus W_N$, $W_i$ irreducible 
$\implies$ $(\chi, \chi_n) = N_n = \abs{\qty{W_i; W_i \cong W_n}}$. \medskip \\

$G$ finite, $n_j$ dimensions of its irreducible representations $\rho_j$ with characters $\chi_j$ 
$\implies$ $\sum_{j \ \text{all irr.\ reps.}} n_j^2 = \abs{G}$ and $\sum_{j \ \text{all irr.\ 
reps.}} n_j \chi_j(g) = \begin{cases}
	\abs{G} ;& g = e \\
	0 ;& \text{otherwise}
\end{cases}$. \medskip \\


\subsection{Class functions}

$f: G \to \C$ is a \textbf{class function} if $f(hgh^{-1}) = f(g)$, $\forall h, g \in G$. \\
$Cl(G) = \qty{f: G \to \C; f \ \text{is a class function}}$ \medskip \\

Characters are class functions. \medskip \\

$G$ finite. Characters of irreducible representations are an orthogonal basis of $Cl(G)$ with 
respect to $\sprod{\bullet, \bullet}$. \medskip \\

\textbf{Conjugacy class}: $C_g = \qty{hgh^{-1}; h \in G}$. \medskip \\

The number of non-equivalent irreducible representations of a finite $G$ is equal to the number of 
conjugacy classes in $G$. \medskip \\


\subsection{Canonical decomposition}

$W_i$ all irreducible representations of finite $G$, $\chi_i$ their characters. $V = \oplus_{i = 
1}^m U_i$ decomposition of $\rho: G \to \GL(V)$ into irreducible representations. Let $V_i$ be the 
direct sum of all 
$U_j$ isomorphic to $W_i$. The \textbf{canonical decomposition} is $V = \oplus_{i = 1}^k V_k$. 
\medskip \\

The canonical decomposition is independent of the initial $U_i$. \\
$p_i = \frac{\dim W_i}{\abs{G}} \sum_{g \in G} \overline \chi_{W_i}(g) \rho(g)$ is a projector $V \to
V_i$. \medskip \\

\section{Manifolds}

\subsection{Topology}

A \textbf{topology on a set} $M$ is a collection $\mathcal{O}$ of subsets of $M$:
\vspace{-0.10cm}
\begin{enumerate}[itemindent=-13pt]
	\itemsep-0.1em 
	\item $\emptyset, M \in \mathcal{O}$, \\
	\item $U, V \in \mathcal{O}$ $\implies$ $U \cap V \in \mathcal{O}$, \\
	\item $\mathcal{U} \subset \mathcal{O}$ $\implies$ $\cup \mathcal{U} \in \mathcal{O}$.
\end{enumerate}
\vspace{-0.11cm}
Elements of $\mathcal{O}$ are called \textbf{open sets}. \\
A \textbf{topological space} is a pair of a set and its topology. \medskip \\

$Z \subseteq M$ is \textbf{closed} if $M \setminus Z$ is open. \medskip \\

$A \subseteq M$ is a \textbf{neighbourhood of a point} $m \in M$ if $\exists$ open $U \subseteq M$: 
$m \in U \subseteq A$. \\
$A \subseteq M$ is a \textbf{neighbourhood of a set} $B \subseteq A$ if $\exists$ open $U \subseteq 
M$: $B \subseteq U \subseteq A$. \medskip \\

\subsection{Homeomorphisms}

$f: X \to Y$ between topological spaces is \textbf{continuous} if $f^{-1}(V) \subseteq X$ is an 
open subset for all open subsets $V \subseteq Y$. \medskip \\

Composition of continuous maps is continuous. \medskip \\

$f: X \to Y$ is \textbf{open} if $f(U) \subseteq Y$ is an open subset for all open subsets $U 
\subseteq X$. \medskip \\

$f: X \to Y$ is a \textbf{homeomorphism} if it is a continuous open bijection. \\
$X$ and $Y$ are \textbf{homeomorphic} ($X \approx Y$) if a homeomorphism between them exists. 
\medskip \\

\subsection{Basis for a topology}

$\mathcal{P} \subset P(M)$ is a \textbf{subbasis for a topology} on $M$ if $\cup \mathcal{P} = M$. 
\\
Subbasis $B$ is a \textbf{basis for a topology} on $M$ if $\forall U, V \in B$, $\forall x \in U 
\cap V$, $\exists W \in B$: $x \in W \subset U \cap V$. \medskip \\

$\mathcal{O} = \qty{\cup U; U \in B}$ is a topology on $M$ generated by $B$. \\
$\mathcal{B} = \qty{\cap U; U \in P, U \ \text{finite and not empty}}$ is a basis for a 
topology on $M$ generated by $P$. \medskip \\

A topological space is \textbf{second-countable} if its topology can be generated by a countable 
basis. \medskip \\

\subsection{Separation axioms}

Topological space $M$. \\
$T_1$: $\forall a, b \in M, a \neq b, \exists$ neighbourhood $U$ of $a$: $b \notin U$. \\
$T_2$: $\forall a, b \in M, a \neq b, \exists$ neighbourhood $U$ of $a$, $\exists$ neighbourhood 
\\ \hspace{12pt} $V$ of $b$: $U \cap V = \emptyset$. \\
$T_3$: $\forall$ closed $B \subset M$, $\forall a \in X \setminus B$, $\exists$ neighbourhood $U$ 
of $a$, $\exists$ \\ \hspace{12pt} neighbourhood $V$ of $B$: $U \cap V = \emptyset$. \\
$T_4$: $\forall$ closed $A, B \subset M$, $A \cap B = \emptyset$, $\exists$ neighbourhood $U$ of 
$A$, $\exists$ \\ \hspace{14pt}neighbourhood $V$ of $B$: $U \cap V = \emptyset$. \medskip \\

$T_1 \land T_4$ (\textbf{normal Hausdorff} space) $\implies T_3$ \\
$T_1 \land T_3$ (\textbf{regular Hausdorff} space) $\implies T_2$ \\
$T_2$ (\textbf{Hausdorff} space) $\implies T_1$ \medskip \\

Metric spaces are normal Hausdorff spaces. \medskip \\

\subsection{Smooth manifolds}

Hausdorff second-countable topological space $M$ is a \textbf{topological manifold} if $\forall m 
\in M$ $\exists$ open neighbourhood homeomorphic to some open subset of $\R^n$. \\
$n$ is the same for all $m$, the \textbf{dimension} of $M$ is $\dim M = n$. 
\medskip \\

$F: V \subseteq \R^n \to W \subseteq \R^n$ is a \textbf{diffeomorphism} if it is a bijective, 
smooth 
and $F^{-1}$ is also smooth. \medskip \\

A \textbf{smooth atlas} on a topological manifold $M$ is a family $\mathcal{U} = \qty{(U_\alpha, 
\varphi_\alpha); \alpha \in A}$, where $U_\alpha \subset M$ is an open set and $\varphi_\alpha: 
U_\alpha \to V_\alpha \approx \R^n$ a homeomorphism, such that
\vspace{-0.10cm}
\begin{enumerate}[itemindent=-13pt]
	\itemsep-0.1em 
	\item $\cup_{\alpha \in A} U_\alpha = M$,
	\item $\forall \alpha, \beta \in A, U_\alpha \cap U_\beta \neq \emptyset$: 
	$\varphi_\beta \circ 
	\varphi_\alpha^{-1}:$ \\ \hspace{-15pt} $\varphi_\alpha(U_\alpha \cap U_\beta) \to 
	\varphi_\beta(U_\alpha \cap U_\beta)$ is a diffeomorphism.
\end{enumerate}
\vspace{-0.11cm}
Elements of an atlas $(U_\alpha, \varphi_\alpha)$ are called \textbf{charts}. \medskip \\

An atlas is \textbf{$\boldsymbol k$-smooth} if all $\varphi_\beta \circ \varphi_\alpha^{-1}: U 
\subset \R^n \to V \subset \R^n$ are k-smooth (partial derivatives up to order $k$ exist). \medskip 
\\

A \textbf{smooth manifold} is a pair of a topological manifold and a smooth atlas on it. \medskip \\

Atlases $\mathcal{U}_1 = \qty{(U_\alpha, \varphi_\alpha); \alpha \in A}$ and $\mathcal{U}_2 = 
\qty{(W_\beta, \psi_\beta); \beta \in B}$ are \textbf{equivalent} ($\mathcal{U}_1 \approx 
\mathcal{U}_2$) if $\forall \alpha \in A$, $\forall \beta \in B$: $\psi_\beta \circ 
\varphi_\alpha^{-1}: \varphi_\alpha(U_\alpha \cap W_\beta) \to \psi_\beta(U_\alpha \cap W_\beta)$ 
is a diffeomorphism. \medskip \\

\subsubsection{Functions on smooth manifolds}

$f: M \to \R$ on a smooth manifold $(M, \mathcal{U})$ is \textbf{smooth} if $\forall \alpha \in A$: 
$f \circ \varphi_\alpha^{-1}: \varphi_\alpha(U_\alpha) \subset \R^n \to \R$ is smooth. \medskip \\

$\gamma: (a, b) \to M$ on a smooth manifold is a \textbf{smooth curve} if 
$\forall \alpha \in A$: $\varphi_\alpha \circ \gamma: (a, b) \to \varphi_\alpha(U_\alpha) \subset 
R^n$ is a smooth curve. \medskip \\

$F: M_1 \to M_2$ between smooth manifolds $(M_i, \mathcal{U}_i)$ is \textbf{smooth} if $\forall 
\alpha \in A_1$, $\forall \beta \in A_2$: $\varphi_\beta^{(2)} \circ F \circ 
\varphi_\alpha^{(1)^{-1}}$: $\varphi_\alpha^{(1)}(U_\alpha^{(1)}) \subset \R^n \to 
\varphi_\beta^{(2)}(F(U_\beta^{(2)})) \subset \R^m$ is smooth. \medskip \\

\subsection{Tangent vectors}

Two smooth curves $\gamma_i: (-\varepsilon, \varepsilon) \to M$ are \textbf{equivalent} ($\gamma_1 
\sim \gamma_2$) if $\gamma_i(0) = m \in M$, $i = 1, 2$, and for some $\alpha \in A$: 
$\eval{\dv{t}}_{t = 0} \varphi_\alpha(\gamma_1(t)) = \eval{\dv{t}}_{t = 0} 
\varphi_\alpha(\gamma_2(t))$. \medskip \\
A \textbf{tangent vector} at $m \in M$ is an equivalence class $\qty[\gamma]$ of smooth curves, 
where $\gamma(0) = m$. \medskip \\

$\dot{\varphi_\alpha(\gamma)} = \eval{\dv{t}}_{t = 0} \varphi_\alpha(\gamma(t))$. \\
Simplest representative: $\qty[\gamma] = \qty[\varphi_\alpha^{-1} \qty(\varphi_\alpha(m) + t 
\dot{\varphi_\alpha(\gamma)})]$\medskip \\

Equivalence of smooth curves is independent of the choice of the chart: 
$\dot{\varphi_\beta(\gamma_1)} = \dot{\varphi_\beta(\gamma_2)}$ $\iff$ 
$\dot{\varphi_\alpha(\gamma_1)} = \dot{\varphi_\alpha(\gamma_2)}$. \\
$\dot{\varphi_\beta(\gamma)} = \qty(D_{\varphi_\alpha(\gamma(0))} \qty(\varphi_\beta \circ 
\varphi_\alpha^{-1})) \qty(\dot{\varphi_\alpha(\gamma)})$ \medskip \\

The set of tangent vectors $T_mM$ on $M$ at $m \in M$ is a vector space of dimension $n = \dim M$. 
\medskip \\

A \textbf{derivative} $D_{m_1}F: T_{m_1} M_1 \to T_{F(m_1)}M_2$ of $F:M_1 \to M_2$ is given by
$(D_{m_1}F)\qty[\gamma] = \qty[F \circ \gamma]$. \\
$\dot{\psi_{\beta}(D_{m_1} F[\gamma])} = D_{\varphi_\alpha(m_1)}\qty(\psi_\beta \circ F \circ 
\varphi_\alpha^{-1})\qty(\dot{\varphi_\alpha(\gamma)})$\medskip \\

\subsubsection{Local coordinates}

\textbf{Coordinate functions} $x_i: \R^n \to \R$ with respect to a basis $\qty{e_i}_{i = 1}^n$ of 
$\R^n$ are given by $p = \sum_{i = 1}^n x_i(p) e_i$. \\
\textbf{Local coordinates} on a smooth manifold $(M, \mathcal{U})$ are given by $x_i^\alpha= x_i 
\circ \varphi_\alpha: U_\alpha \to \R$. \medskip \\

$\pdv{x_i^\alpha(m)} = \qty[\varphi_\alpha^{-1}\qty(\varphi_\alpha(m) + t e_i)]$ \\ 
$\qty{\pdv{x_i^\alpha(m)}}_{i = 1}^n$ is a basis of $T_mM$. \\
$\qty(D_m \varphi_\alpha) \qty(\sum_{i = 1}^n v_i(m) \pdv{x_i^\alpha(m)}) = \sum_{i = 1}^n v_i(m) 
e_i$ \\
$\qty[\gamma] = \sum_{i = 1}^n \dot x_i^\alpha(\gamma(0)) \pdv{x_i^\alpha(\gamma(0))} = \sum_{i = 
1}^n \dot \gamma_i^\alpha(0) \pdv{x_i^\alpha(\gamma(0))}$ \medskip \\

An isomorphism between tangent spaces in two different charts is 
$D_{\varphi_\alpha(m)}(\varphi_\beta \circ \varphi_\alpha^{-1}): T_{\varphi_\alpha(m)}\R^n \to 
T_{\varphi_\beta(m)}\R^n$. \medskip \\

\subsubsection{Tangent vectors as differential operators}

The \textbf{directional derivative} $V$ of $f: M \to \R$ in the direction $v \in T_m M$ is given by 
$(V(f))(m) = \eval{\dv{t}}_{t = 0} f(\gamma(t))$, where $\gamma:(-\varepsilon, \varepsilon) \to M$, 
$\gamma(0) = m$ and $\qty[\gamma] = v$. \medskip \\

$M$ smooth manifold, $p \in U \subset M$. A \textbf{1st order differential operator} $A_p: 
C^\infty(U) \to \R$ at point $p$ is a map satisfying:
\vspace{-0.10cm}
\begin{enumerate}[itemindent=-13pt]
	\itemsep-0.1em 
	\item linearity: $A_p(\alpha f + \beta g) = \alpha A_p(f) + \beta A_p(g)$, \\
	\item Leibnitz rule: $A_p(fg) = A_p(f) g(p) + f(p) A_p(g)$.
\end{enumerate}
\vspace{-0.11cm} \medskip 

Every partial differential operator of the 1st order is a directional derivative in the direction 
of some uniquely determined tangent vector. \medskip \\

We can treat tangent vectors as 1st order differential operators. For local coordinates, we 
define $\pdv{x_i^\alpha(m)}: C^\infty(U) \to \R, \pdv{f}{x_i^\alpha(m)} = \pdv{f \circ 
\varphi_\alpha^{-1}}{x_i}\qty(\varphi_\alpha(m))$. \medskip \\

\subsection{Submersions, immersions, embeddings}

$M, N$ smooth manifolds, $\dim N < \dim M$. $N$ is a \textbf{submanifold} of $M$ if $\forall p \in 
N$, $\exists$ chart $(U, \varphi)$, $p \in U$ on $M$, such that $\eval{\varphi}_{U \cap N}$ is a 
chart on $N$. 
\\
More precisely, the chart has to have the form: \\
$\varphi(U) = V \approx \R^n$ \\
$\varphi(U \cap N) = V \cap \qty(\R^k \times \qty{0}^{n - k})$ \medskip \\


$M, N$ smooth manifolds, $\dim M \geq \dim N$. $F: M \to N$ is a \textbf{submersion} if $D_m 
F: T_m M \to T_{F(m)}N$ is surjective $\forall m \in M$. \medskip \\

$F: M \to N$ submersion $\implies$ for almost every $p \in N$, the set $F^{-1}(p) \subset M$ is a 
submanifold of dimension $\dim M - \dim N$. \medskip \\

$M, N$ smooth manifolds, $\dim M \leq \dim N$. $F: M \to N$ is an \textbf{immersion} if $D_m 
F: T_m M \to T_{F(m)}N$ is injective $\forall m \in M$. \medskip \\

$M, N$ smooth manifolds. A map $F: N \to M$ is an \textbf{embedding} if it is an immersion and 
$F(N)$ is a submanifold of $M$. \medskip \\

\subsection{Vector bundles}

$E, M$ smooth manifolds, $\Pi: E \to M$ a smooth map, such that
\vspace{-0.10cm}
\begin{enumerate}[itemindent=-13pt]
	\itemsep-0.1em 
	\item $\forall p \in M$, $\Pi^{-1}(p) = V_p \subset E$ is linearly isomorphic to $\R^n$, \\
	\item $\forall p \in M$, $\exists$ neighbourhood $p \in U \subset M$, such that $\exists$ \\ 
	\hspace{-15pt}diffeomorphism $\tau_U: \Pi^{-1}(U) \to U \times V$, for some
	$V \approx \R^n$ \\ \hspace{-15pt}and $\eval{\tau_U}_{\Pi^{-1}(m)}: \Pi^{-1}(m) \to m
	\times V$ is linear $\forall m \in U$
\end{enumerate}
\vspace{-0.11cm}
Then $\Pi$ is called a \textbf{vector bundle} of rank $n$ over $M$. \medskip \\

$\Pi^{-1}(p)$ are \textbf{fibres}, $\eval{\tau_U}_{\Pi^{-1}(p)}$ are \textbf{trivializations}. 
\medskip \\

$\Pi: E \to M$ a vector bundle. $E$ is \textbf{trivial} if
$\exists \tau: E \to M \times V$,
diffeomorphism, linear for each fibre. \medskip \\

Given a smooth atlas $\mathcal{U}$ on $M$ and trivializations $\tau_{U_\alpha}: \Pi^{-1}(U_\alpha) 
\to U_\alpha \times V$, a smooth atlas on $E$ can be constructed from charts 
$(\Pi^{-1}(U_\alpha), \qty(\varphi_\alpha \times \text{id}) \circ \tau_{U_\alpha}: 
\Pi^{-1}(U_\alpha) \to \R^{\dim M + n})$. \medskip \\

\subsubsection{Transition maps and cocyles}

$\tau_{\beta \alpha} = \tau_\beta \circ \tau_\alpha^{-1} = \tau_{U_\beta} \circ 
\tau_{U_\alpha}^{-1}: U_\alpha \cap 
U_\beta \times V \to U_\alpha \cap U_\beta \times V$, \\ $\qty(\Pi(m), 
\qty(v_1^\alpha(m), \dots, v_n^\alpha(m))) \mapsto \qty(\Pi(m), \qty(v_1^\beta(m), \dots, 
v_n^\beta(m)))$ \\
$\qty(\tau_\beta \circ \tau_\alpha^{-1})(p, \vb v) = (p, g_{\beta\alpha}(p)(\vb v))$ \\
$g_{\beta\alpha}: U_\alpha \cap U_\beta \to \text{GL}(V) = \text{GL}(n; \mathbb{F})$ \\
$g_{\beta\alpha}$ are called \textbf{transition maps}, trivial vector bundles can be glued together 
by them. \medskip \\

$\tau_{\beta \alpha} = \tau_{\alpha \beta}^{-1}$ \\
$\tau_{\gamma \beta} \circ \tau_{\beta \alpha} \circ \tau_{\alpha \gamma} = \text{id}$ \medskip \\

$M$ smooth manifold with smooth atlas $\mathcal{U} = \qty{(U_\alpha, \varphi_\alpha); \alpha \in 
A}$. A \textbf{cocyle} on $M$ is a family $\{g_{\beta\alpha}: U_\alpha \cap U_\beta \to 
\text{GL}(V);$ $U_\alpha \cap U_\beta \neq \emptyset\}$, where
\vspace{-0.10cm}
\begin{enumerate}[itemindent=-13pt]
	\itemsep-0.1em 
	\item $g_{\beta \alpha}(m) = g_{\alpha \beta}^{-1}(m)$, \qquad $\forall m \in U_\alpha \cap 
	U_\beta$ ,
	\item $g_{\gamma \beta}(m) \circ g_{\beta \alpha}(m) \circ g_{\alpha \gamma}(m) = \text{id}$, 
	\qquad $\forall m \in U_\alpha \cap U_\beta \cap U_\gamma$.
\end{enumerate}
\vspace{-0.11cm}
Transition maps form a cocyle. \medskip \\

There exists an unique vector bundle $\Pi: E \to M$ constructed from a given cocycle, if $M$ is a 
smooth manifold with contractible $U_\alpha$. \medskip \\

\subsubsection{Smooth sections}

$\Pi: E \to M$ vector bundle. Smooth $s: M \to E$, such that $s(m) \in \Pi^{-1}(m)$ is a 
\textbf{section} of $E$. \medskip \\

$\Pi: E \to M$ vector bundle, $U \subset M$. Smooth $s: U \to \Pi^{-1}(U)$, such that $s(m) \in 
\Pi^{-1}(m)$ is a \textbf{local section}. \medskip \\

Local sections $\qty{s_i: U \to \Pi^{-1}(U)}_{i = 1}^n$ are \textbf{linearly independent} if 
$\qty{s_i(m) \in \Pi^{-1}(m)}_{i = 1}^n$ are linearly independent $\forall m \in U$. \medskip \\

$\rank E = n$ linearly independent local sections $\qty{s_i: U \to 
\Pi^{-1}(U)}_{i = 1}^n$ define a local trivialization of $E$ over $U$ $\tau_U(m) = \qty(\Pi(m), 
\qty(a_1(m), \dots, a_n(m)))$, where $m = \sum_{j = 1}^n a_j(m) s_j(\Pi(m))$. \\
Sets of local bases composed of local sections are called \textbf{local gauges} or \textbf{frames}. 
\medskip \\

A local trivialization $\tau_U: \Pi^{-1}(U) \to U \times \mathbb{F}^n$ determines a set of linearly 
independent local sections $s_i: U \to \Pi^{-1}(U)$, where $s_i(m) = \tau^{-1}(m, \vb e_i)$. 
\medskip \\

If $n = \rank E$ global linearly independent sections exists, then $E$ is a trivial bundle. 
\medskip \\

Every local section $o: U \to \Pi^{-1}(U)$ can be expressed as: \\
$o(m) = \sum_{i = 1}^n A_i(m) s^\alpha_i(m) = \sum_{i = 1}^n B_i(m) s^\beta_i(m)$,
where $\qty{s_i^\alpha}_{i = 1}^n$, $\qty{s_i^\beta}_{i = 1}^n$ are linearly independent and $\rank 
E = n$. \\
$s_i^\alpha(m) = \sum_{j = 1}^n g_{\beta\alpha}(m)_{ji} s_j^\beta(m)$ \\
$B_j(m) = \sum_{i = 1}^n g_{\beta \alpha}(m)_{ji} A_i(m)$ \medskip \\

\subsubsection{Tangent bundle}

\textbf{Tangent bundle} of $n$ dimensional smooth manifold $M$ with atlas $\mathcal{U} = 
\qty{(U_\alpha, 
\varphi_\alpha); \alpha \in A}$ is $TM = \bigsqcup_{m \in M} T_m M$. \medskip \\

$\tau_\alpha: TU_\alpha \to \varphi_\alpha(U_\alpha) \times \R^n$ \quad (trivialization in local 
coord.) \\
$\tau_\alpha(v_m) = \qty(\varphi_\alpha(m), \qty(D_m \varphi_\alpha)(v_m))$ \medskip \\

\textbf{Topology of} $TM$: $V \subset TU_\alpha$ is open if $\tau_\alpha(V)$ is open in 
$\varphi_\alpha(U_\alpha) \times \R^n$. \medskip \\

\textbf{Smooth atlas of} $TM$: $T \mathcal{U} = \qty{(TU_\alpha, \tau_\alpha); \alpha \in A}$. 
\medskip \\

\textbf{Transition maps of} $TM$: \\
$\tau_\beta \circ \tau_\alpha^{-1}: \varphi_\alpha(U_\alpha \cap U_\beta) \times \R^n \to 
\varphi_\beta (U_\alpha \cap U_\beta) \times \R^n$ \\
$\tau_\beta \circ \tau_\alpha^{-1} = \qty(\varphi_\beta \circ \varphi_\alpha^{-1}, D_m 
\varphi_\beta \circ D_{\varphi_\alpha(m)}\varphi_\alpha^{-1}) =$ \\ \hspace{31pt} 
$=\qty(\varphi_\beta \circ 
\varphi_\alpha^{-1}, D_{\varphi_\alpha(m)}(\varphi_\beta \circ \varphi_\alpha^{-1}))$ \medskip \\

\textbf{Cocycle of} $TM$: $\qty{m \mapsto D_{\varphi_\alpha(m)}(\varphi_\beta \circ 
\varphi_\alpha^{-1}); m \in U_\alpha \cap U_\beta \neq \emptyset}$ \medskip \\

\textbf{Local frames of} $TM$: \\
$s_i^\alpha(m) = \pdv{x_i^\alpha(m)}$ \medskip \\

\subsection{Vector fields}

A \textbf{vector field} $V: M \to TM$ on $M$ is a section of $TM$. \medskip \\

$V(m) = \sum_{i = 1}^n a_i(m) \pdv{x_i^\alpha(m)}$ \\
Vector field yields a partial differential operator $V: C^k(M) \to C^{k - 1}(M)$ where $V(f)(m) = 
\sum_{i = 1}^n a_i(m) \pdv{f \circ 
\varphi_\alpha^{-1}}{x_i}(\varphi_\alpha(m))$. \medskip \\

The set of all $C^k$ vector fields on $M$: $\Gamma^k(M)$. $\Gamma(M) = \Gamma^\infty(M)$. \\
$\Gamma^k(M)$ is a vector space. For $V, W \in \Gamma^k(M)$ \\
$(V + W)(m) = V(m) + W(m)$ \\
$(\alpha V)(m) = \alpha V(m)$ \medskip \\

\subsubsection{Flows}

$V \in \Gamma(M)$ and $V(m) \neq 0$. $\gamma_m: (- \varepsilon, \varepsilon) \to M$ is an 
\textbf{integral curve} of $V$ through $m$ if $\gamma_m(0) = m$ and $\dot \gamma_m(t) = 
V(\gamma_m(t))$. \medskip \\

$\forall V \in \Gamma(M)$ an integral curve exists locally. \medskip \\

The \textbf{flow} of a vector field $V$ through $U \subset M$ is a one parameter family of maps 
$\Phi_t(m) = \gamma_m(t)$. \medskip \\

Maps $\Phi_t$ are diffeomorphisms (for small enough $t$). \\
$\Phi_{t + s} = \Phi_t \circ \Phi_s$ (for small enough $t$, $s$). \\

\subsubsection{Lie derivative}

\textbf{Lie derivative} of $f: M \to \R$ is the directional derivative. \medskip \\

\textbf{Lie derivative} of $W \in \Gamma(M)$ in the direction of $V \in \Gamma(M)$ at $m \in M$ is 
$\mathcal{L}_V(W)(m) = \eval{\dv{t}}_{t = 0}\qty(D_m \Phi^V_t)^{-1}(W(\gamma^V_m(t)))$. \medskip \\

In local coordinates: \\
$\mathcal{L}_V(W)(p) = \comm{V}{ W}(p) = D_pW(V(p)) - D_pV(W(p))$ \medskip \\

Treating $V$, $W$ as partial differential operators: \\
$\mathcal{L}_V(W)(f) = \comm{V}{ W}(f) = V(W(f)) - W(V(F))$
$\comm{V}{ W}(f)(m) = \eval{\frac{\dd[2]}{\dd{t} 
\dd{s}}}_{t,s=0} \qty[f\qty(\Phi^W_s\qty(\Phi_t^V(m))) - f\qty(\Phi^V_t\qty(\Phi_s^W(m)))]$ 
\medskip \\

$\comm{V}{ \comm{W}{ Z}} + \comm{Z}{ \comm{V}{ W}} + \comm{W}{ \comm{Z}{ V}} = 0$ \\
$\comm{V}{ W} = - \comm{W}{ V}$ \medskip \\

A vector space $A$ equipped with a bilinear operation $\comm{\bullet}{ \bullet}: A \times A \to A$, 
which satisfies
\vspace{-0.10cm}
\begin{enumerate}[itemindent=-13pt]
	\itemsep-0.1em 
	\item $\comm{V}{ W} = -\comm{W}{ V}$, \\
	\item $\comm{V}{ \comm{W}{ Z}} + \comm{Z}{ \comm{V}{ W}} + \comm{W}{ \comm{Z}{ V}} = 0$,
\end{enumerate}
\vspace{-0.11cm}
is called a \textbf{Lie algebra}. \medskip \\

\subsubsection{Subbundles}

$\Pi_E: E \to M$ vector bundle. A smooth manifold $F \subset E$ is a \textbf{vector subbundle} of 
$E$ if $F$ is a vector bundle $\Pi_F:F \to M$ and $\forall m \in M: \Pi_F^{-1}(m) \subset 
\Pi_E^{-1}(m)$ (vector subspace). \medskip \\

A vector subbundle of $TM$ is called a \textbf{distribution} on $M$. \medskip \\

\textbf{Integral submanifold} of distribution $D$ on $U$ is a submanifold $N_D \subset U$, such 
that $\forall m \in N_D: T_mN_D = D(m) \subset T_mU=T_mM$. \\
Vector fields are $1$D distributions with integral curves as integral manifolds. \medskip \\

If $D$ has integral submanifolds, then an integral submanifold $N_D$ is fixed by a choice of $m \in 
M$, such that $m \in N_D$. \medskip \\

\textbf{Frobenius theorem}: $D$ local distribution on $M$ and $D = \text{span}\qty{V_1, \dots, 
V_r}$. If 
$\forall i,j = 1, \dots, r:$ $\comm{V_i}{ V_j}(m) = \sum_{k = 1}^r a_k(m) V_k(m)$, then $D$ has an 
integral submanifold through every $m \in M$. \medskip \\

\subsection{Differential forms}

$V$ vector space. \\
$S^2 V = \text{span}\qty{e_1 \otimes e_2 + e_2 \otimes e_1; \qty{e_i} \text{ basis of } V}$ \\
$\Lambda^2 V = \text{span}\qty{e_1 \otimes e_2 - e_2 \otimes e_1; \qty{e_i} \text{ basis of } V}$ \\
\textbf{Symmetric product}: $v_1 \os v_2 = v_1 \otimes v_2 + v_2 \otimes v_1 \in S^2 V$ \\
\textbf{Wedge (anti-sym.) pr.}: $v_1 \wedge v_2 = v_1 \otimes v_2 - v_2 \otimes v_1 \in 
\Lambda^2 V$ \medskip \\

For $a, b \in \R^3$: $a \wedge b = (a \times b)_1 e_1 \wedge e_2 + (a \times 
b)_2 e_3 \wedge e_1 + (a \times b)_3 e_2 \wedge e_3$ \medskip \\

$a_1 \wedge a_2 \wedge \cdots \wedge a_k = \sum_{\sigma \in S_k} \abs{\sigma} a_{\sigma(1)} \otimes 
a_{\sigma(2)} \otimes \cdots \otimes a_{\sigma(k)}$ \\
$a_1 \wedge a_2 \wedge \cdots \wedge a_k = \abs{\sigma} \qty(a_{\sigma(1)} \wedge
a_{\sigma(2)} \wedge\cdots \wedge a_{\sigma(k)})$ \\
$\Lambda^kV = \text{span}\{e_{i_1} \wedge e_{i_2} \wedge \cdots \wedge e_{i_k}; i_1 < \cdots < 
i_k, \qty{e_i} \text{ basis of } V\}$ \\
$\dim \Lambda^k V = \binom{\dim V}{k}$ \medskip \\

\subsubsection{Cotangent bundle}

The \textbf{dual space} of the vector space $V$ is $V^* = \qty{f: V \to \mathbb{F}; f \text{ 
linear}}$. \medskip \\

The \textbf{dual bundle} $\tilde \Pi: E^* \to M$ of a bundle $\Pi: E \to M$ is a bundle whose 
fibres $\tilde \Pi^{-1}(m)$ are dual spaces of the fibres $\Pi^{-1}(m)$: $\tilde \Pi^{-1}(m) = 
(\Pi^{-1}(m))^*$. \medskip \\

The \textbf{cotangent bundle} is the dual bundle of the tangent bundle $T^* M = (TM)^*$. \medskip \\

\subsubsection{Differential 1-forms}

A smooth section of $T^*M$ is called a \textbf{differential 1-form}. \\
Space of differential 1-forms: $\Omega^1(M) = \Gamma(T^*M). $\medskip \\

The \textbf{dual frame} to the local frame of $TU_\alpha$ $\qty(\qty{\pdv{x_i^\alpha(m)}})$ is denoted by
$\qty{\dd x_1^\alpha(m), \dots, \dd x_n^\alpha(m)}$. \\
$\dd x_i^\alpha(m)\qty(\pdv{x_j^\alpha(m)}) = \delta_{ij}$ \medskip \\

$V(m) = \sum_{j =1}^n a_j^\alpha \pdv{x_j^\alpha(m)} \in \Gamma(M)$ \\
$\omega_m = \omega(m) = \sum_{j = 1}^n b_j^\alpha(m) \dd x_j^\alpha(m) \in \Omega^1(M)$ \\
$\omega_m(V(m)) = \omega(V)(m) = \sum_{j = 1}^\alpha a_j^\alpha(m) b_j^\alpha(m)$ \\ 
$\qty[\qty(D_{\varphi_\alpha(m)} (\varphi_\beta \circ \varphi_\alpha^{-1}))^T]^{-1} 
\mqty(b_1^\alpha(m) \\ \vdots \\ b_n^\alpha(m ))= \mqty(b_1^\beta(m) \\ \vdots \\ b_n^\beta(m))$ 
\bigskip \\

For $f: M \to \R$ we define $\dd{f} = \sum_{j = 1}^n \pdv{f \circ 
\varphi_\alpha^{-1}}{x_j^\alpha(m)} \dd x_j^\alpha(m)$.
$\dd{f}(V(m)) = V(f)(m)$ \medskip \\

1-form $\omega \in \Omega^1(M)$ for which there exists a function $f: M \to \R$, such that $\dd f = 
\omega$, is called an \textbf{exact 1-form}. \medskip \\

\textbf{Integral of a 1-form} $\omega \in \Omega^1(M)$ along smooth $\gamma:[a, b] \to M$: 
$\int_\gamma \omega = \int_a^b \omega_{\gamma(t)}(\dot \gamma(t)) \dd{t}$. \medskip \\

\subsubsection{Differential k-forms}

\textbf{Tensor product of bundles}: $\Pi^{\otimes k}: E^{\otimes k} \to M$, where 
$\qty(\Pi^{\otimes k})^{-1}(m) = \qty(\Pi^{-1}(m))^{\otimes k}$. \medskip \\

$\Lambda^k(T^*M)$, called the \textbf{k-th exterior power of} $T^* M$, is the subbundle of
$(T^*M)^{\otimes k}$, given by $\hat \Pi: \Lambda^k(T^*M) \to M$, where the fibre is $\hat
\Pi^{-1}(m) = \Lambda^k T_m^*M$. \medskip \\

A \textbf{differential k-form} on $M$ is a smooth section of $\Lambda^k(T^* M)$. \\
Space of differential k-forms: $\Omega^k(M)$. \\
Local expression: $\omega_m = \sum_I a_{i_1, \dots, i_k}(\varphi_\alpha(m)) \dd x_{i_1}^\alpha 
\wedge \cdots \wedge \dd x_{i_k}^\alpha$ \medskip \\

$\omega_i \in V^*, \xi_i \in V, i = 1, \dots, k$: \\$\qty(\omega_1 \wedge \cdots \wedge 
\omega_k)(\xi_1, \dots, \xi_k) = \det \mqty(\omega_1(\xi_1) & \cdots & \omega_1(\xi_k) \\ \vdots & 
\ddots & \vdots \\ \omega_k(\xi_1) & \cdots & \omega_k(\xi_k))$ \medskip \\

$\omega_k \in \Omega^k(M), \omega_l \in \Omega^l(M), \xi_i \in \Gamma(TM)$, then $\qty(\omega_k
\wedge \omega_l)(\xi_1, \dots, \xi_k, \dots, \xi_{k + l}) = \sum_{\sigma \in S_{k + l}} \abs{\sigma}
\omega_k(\xi_{\sigma(1)}, \dots, \xi_{\sigma(k)}) \omega_l(\xi_{\sigma(k + 1)}, \dots, \xi_{\sigma(k
+ l)})$ \medskip \\


\subsubsection{Transformation rules}

$\omega = \sum_{i = 1}^n \omega_i^\alpha \dd x_i^\alpha = \sum_{i = 1}^n \omega_i^\beta \dd 
x_i^\beta \in \Omega^1(M)$ \\
$\omega_j^\alpha = \sum_{i = 1}^n \pdv{x_i^\beta}{x_j^\alpha} \omega_i^\beta$ \\
$\dd x_j^\alpha = \sum_{i = 1}^n \pdv{x_j^\alpha}{x_i^\beta} \dd x_i^\beta$ \\
$\dd x_{i_1}^\beta \wedge \cdots \wedge \dd x_{i_k}^\beta = \sum_{J} 
\pdv{x_{i_1}^\beta}{x_{j_1}^\alpha} \cdots \pdv{x_{i_k}^\beta}{x_{j_k}^\alpha} \dd x_{j_1}^\alpha 
\wedge \cdots \wedge \dd x_{j_k}^\alpha$

\subsubsection{Pull-back of a differential form}

Let $f: M \to C$, $\omega \in \Omega^k(C)$. The \textbf{pullback} $f^*(\omega)$ of the differential
form $\omega$ is given by $(f^*(\omega))_m\qty(V_1(m), \dots, V_k(m)) =
\omega_{f(m)}\qty(D_mf(V_1(m)), \dots, D_mf(V_k(m)))$. \medskip \\

$\omega_k \in \Omega^k(C), \omega_l \in \Omega^l(C), f: M \to C$, then $f^*(\omega_1 \wedge \omega_2)
= f^*(\omega_1) \wedge f^*(\omega_2)$. \medskip \\

If $\omega \in \Omega^1(C)$ is locally given by $\omega = \sum_{j = 1}^m \alpha_j \dd y_j$, then
$f^*(\omega) = \sum_{j = 1}^m \alpha_j \dd f_j$. \medskip \\

If $\omega \in \Omega^k(C)$ is locally given by $\omega = \sum_I \alpha_I \dd y_{i_1} \wedge \cdots
\wedge \dd y_{i_k}$, then $f^*(\omega) = \sum_I \alpha_I \dd f_{i_1} \wedge \cdots \wedge \dd
f_{i_k}$. \medskip \\

\subsubsection{Exterior derivative}

For $\omega \in \Omega^k(M)$ we define $\deg \omega = k$. \medskip \\

There exists only one $\R$-linear op.\ $\dd: \Omega^k \to \Omega^{k + 1}$, for which 
\vspace{-0.11cm}
\begin{enumerate}[itemindent=-13pt]
	\itemsep-0.1em 
	\item $\dd(\alpha \wedge \beta) = \dd \alpha \wedge \beta + \qty(-1)^{\deg \alpha} \alpha \wedge
	\dd \beta$, \\
	\item for $f \in \Omega^0(M) = C(M)$: $\dd f(V) = V(f)$, \\
	\item $\dd \circ \dd = 0$.
\end{enumerate}
\vspace{-0.11cm}
We call this operator the \textbf{exterior derivative}. \medskip \\

Let $f: M \to C$, then $\dd_M (f^*(\omega)) = f^*(\dd_C \omega)$. \medskip \\

If $\omega \in \Omega^k(M)$ is locally given by $\omega = \sum_I \alpha_I \dd x_{i_1} \wedge \cdots
\wedge \dd x_{i_k}$, then $\dd \omega = \sum_I \dd \alpha_I \wedge \dd x_{i_1} \wedge \cdots
\wedge \dd x_{i_k}$. \medskip \\

\end{multicols}
\end{document}
